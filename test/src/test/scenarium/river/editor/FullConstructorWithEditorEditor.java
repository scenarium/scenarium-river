package test.scenarium.river.editor;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import io.beanmanager.editors.PropertyEditor;

import test.scenarium.river.modelTest.FullConstructorWithEditor;

public class FullConstructorWithEditorEditor extends PropertyEditor<FullConstructorWithEditor> {
	@Override
	public boolean hasCustomEditor() {
		return false;
	}

	@Override
	public FullConstructorWithEditor readValue(DataInput raf) throws IOException {
		long versionId = raf.readShort();
		if (versionId == 1L) {
			boolean testBoolean = raf.readBoolean();
			byte testByte = raf.readByte();
			char testChar = raf.readChar();
			short testShort = raf.readShort();
			int testInt = raf.readInt();
			long testLong = raf.readLong();
			float testFloat = raf.readFloat();
			double testDouble = raf.readDouble();
			String testString = raf.readUTF();
			raf.readChar(); // remove '\n' ...
			return new FullConstructorWithEditor(testBoolean, testByte, testChar, testShort, testInt, testLong, testFloat, testDouble, testString);
		}
		throw new IOException("FullConstructorWithEditor deserialisation version not supported: decode=" + versionId + " require<=" + FullConstructorWithEditor.serialVersionUID);
	}

	@Override
	public void writeValue(DataOutput raf, FullConstructorWithEditor value) throws IOException {
		raf.writeShort((int) FullConstructorWithEditor.serialVersionUID);
		raf.writeBoolean(value.isTestBoolean());
		raf.writeByte(value.getTestByte());
		raf.writeChar(value.getTestChar());
		raf.writeShort(value.getTestShort());
		raf.writeInt(value.getTestInt());
		raf.writeLong(value.getTestLong());
		raf.writeFloat(value.getTestFloat());
		raf.writeDouble(value.getTestDouble());
		raf.writeUTF(value.getTestString());
		raf.writeChar('\n'); // this is to facilitate the inspection of the recorded data...
	}

	@Override
	public String getAsText() {
		// No need in the current test
		return "";
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		// No need in the current test
		setValue(null);
	}
}
