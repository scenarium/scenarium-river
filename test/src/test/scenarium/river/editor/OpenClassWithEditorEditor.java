package test.scenarium.river.editor;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import io.beanmanager.editors.PropertyEditor;

import test.scenarium.river.modelTest.OpenClassWithEditor;

public class OpenClassWithEditorEditor extends PropertyEditor<OpenClassWithEditor> {
	@Override
	public boolean hasCustomEditor() {
		return false;
	}

	@Override
	public OpenClassWithEditor readValue(DataInput raf) throws IOException {
		long versionId = raf.readShort();
		if (versionId == 1L) {
			OpenClassWithEditor value = new OpenClassWithEditor();
			value.testBoolean = raf.readBoolean();
			value.testByte = raf.readByte();
			value.testChar = raf.readChar();
			value.testShort = raf.readShort();
			value.testInt = raf.readInt();
			value.testLong = raf.readLong();
			value.testFloat = raf.readFloat();
			value.testDouble = raf.readDouble();
			value.testString = raf.readUTF();
			raf.readChar(); // remove '\n' ...
			return value;
		}
		throw new IOException("OpenClassWithEditor deserialisation version not supported: decode=" + versionId + " require<=" + OpenClassWithEditor.serialVersionUID);
	}

	@Override
	public void writeValue(DataOutput raf, OpenClassWithEditor value) throws IOException {
		raf.writeShort((int) OpenClassWithEditor.serialVersionUID);
		raf.writeBoolean(value.testBoolean);
		raf.writeByte(value.testByte);
		raf.writeChar(value.testChar);
		raf.writeShort(value.testShort);
		raf.writeInt(value.testInt);
		raf.writeLong(value.testLong);
		raf.writeFloat(value.testFloat);
		raf.writeDouble(value.testDouble);
		raf.writeUTF(value.testString);
		raf.writeChar('\n'); // this is to facilitate the inspection of the recorded data...
	}

	@Override
	public String getAsText() {
		// No need in the current test
		return "";
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		// No need in the current test
		setValue(null);
	}
}
