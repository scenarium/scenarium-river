package test.scenarium.river.editor;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import io.beanmanager.editors.PropertyEditor;

import test.scenarium.river.modelTest.FixeSizeClass;
import test.scenarium.river.modelTest.FullConstructorWithEditor;

public class FixeSizeClassEditor extends PropertyEditor<FixeSizeClass> {
	@Override
	public boolean hasCustomEditor() {
		return false;
	}

	@Override
	public FixeSizeClass readValue(DataInput raf) throws IOException {
		long versionId = raf.readShort();
		if (versionId == 1L) {
			boolean testBoolean = raf.readBoolean();
			int testInt = raf.readInt();
			long testLong = raf.readLong();
			float testFloat = raf.readFloat();
			raf.readChar(); // remove '\n' ...
			return new FixeSizeClass(testBoolean, testInt, testLong, testFloat);
		}
		throw new IOException("FullConstructorWithEditor deserialisation version not supported: decode=" + versionId + " require<=" + FullConstructorWithEditor.serialVersionUID);
	}

	@Override
	public void writeValue(DataOutput raf, FixeSizeClass value) throws IOException {
		raf.writeShort((int) FixeSizeClass.serialVersionUID);
		raf.writeBoolean(value.isTestBoolean());
		raf.writeInt(value.getTestInt());
		raf.writeLong(value.getTestLong());
		raf.writeFloat(value.getTestFloat());
		raf.writeChar('\n'); // this is to facilitate the inspection of the recorded data...
	}

	@Override
	public String getAsText() {
		// No need in the current test
		return "";
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		// No need in the current test
		setValue(null);
	}
}
