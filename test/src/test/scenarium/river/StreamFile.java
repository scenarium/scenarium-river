/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     DUPIN Edouard - initial API and implementation
 ******************************************************************************/
package test.scenarium.river;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

import javax.vecmath.Color4f;
import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4d;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point2d;
import javax.vecmath.Point2f;
import javax.vecmath.Point2i;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Point3i;
import javax.vecmath.Point4d;
import javax.vecmath.Point4f;
import javax.vecmath.Point4i;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4d;
import javax.vecmath.Vector4f;

import io.beanmanager.consumer.EditorConsumer;
import io.beanmanager.editors.PropertyEditorManager;
import io.beanmanager.editors.primitive.number.ControlType;
import io.scenarium.pluginManager.ModuleManager;
import io.scenarium.river.consumer.DataStreamConsumer;
import io.scenarium.river.consumer.FileStreamConsumer;
import io.scenarium.river.datastream.DataStreamManager;
import io.scenarium.river.filerecorder.FileStreamManager;
import io.scenarium.river.filerecorder.reader.FileStreamReader;
import io.scenarium.river.filerecorder.reader.PropertyStreamReader;
import io.scenarium.river.filerecorder.reader.SerializableObjectStreamReader;
import io.scenarium.river.filerecorder.reader.StreamableObjectStreamReader;
import io.scenarium.river.filerecorder.recorder.FileStreamRecorder;
import io.scenarium.river.filerecorder.recorder.PropertyStreamRecorder;
import io.scenarium.river.filerecorder.recorder.SerializableObjectStreamRecorder;
import io.scenarium.river.filerecorder.recorder.StreamableObjectStreamRecorder;
import io.scenarium.river.internal.Log;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;

import javafx.scene.paint.Color;
import test.scenarium.river.datastream.input.FixeSizeClassInputStream;
import test.scenarium.river.datastream.input.FullConstructorWithStreamerInputStream;
import test.scenarium.river.datastream.input.OpenClassWithStreamerInputStream;
import test.scenarium.river.datastream.input.PrivateConstructorWithStreamerInputStream;
import test.scenarium.river.datastream.input.SerialisationMultipleVersionInputStream;
import test.scenarium.river.datastream.output.FixeSizeClassOutputStream;
import test.scenarium.river.datastream.output.FullConstructorWithStreamerOutputStream;
import test.scenarium.river.datastream.output.OpenClassWithStreamerOutputStream;
import test.scenarium.river.datastream.output.PrivateConstructorWithStreamerOutputStream;
import test.scenarium.river.datastream.output.SerialisationMultipleVersionOutputStream;
import test.scenarium.river.editor.FixeSizeClassEditor;
import test.scenarium.river.editor.FullConstructorWithEditorEditor;
import test.scenarium.river.editor.OpenClassWithEditorEditor;
import test.scenarium.river.editor.PrivateConstructorWithEditorEditor;
import test.scenarium.river.filerecorder.reader.FixeSizeClassReader;
import test.scenarium.river.filerecorder.reader.FullConstructorWithFileRecorderReader;
import test.scenarium.river.filerecorder.reader.OpenClassWithFileRecorderReader;
import test.scenarium.river.filerecorder.reader.PrivateConstructorWithFileRecorderReader;
import test.scenarium.river.filerecorder.recorder.FixeSizeClassRecorder;
import test.scenarium.river.filerecorder.recorder.FullConstructorWithFileRecorderRecorder;
import test.scenarium.river.filerecorder.recorder.OpenClassWithFileRecorderRecorder;
import test.scenarium.river.filerecorder.recorder.PrivateConstructorWithFileRecorderRecorder;
import test.scenarium.river.modelTest.FixeSizeClass;
import test.scenarium.river.modelTest.FullConstructor;
import test.scenarium.river.modelTest.FullConstructorSerialisable;
import test.scenarium.river.modelTest.FullConstructorWithEditor;
import test.scenarium.river.modelTest.FullConstructorWithFileRecorder;
import test.scenarium.river.modelTest.FullConstructorWithStreamer;
import test.scenarium.river.modelTest.OpenClass;
import test.scenarium.river.modelTest.OpenClassSerialisable;
import test.scenarium.river.modelTest.OpenClassWithEditor;
import test.scenarium.river.modelTest.OpenClassWithFileRecorder;
import test.scenarium.river.modelTest.OpenClassWithStreamer;
import test.scenarium.river.modelTest.PrivateConstructor;
import test.scenarium.river.modelTest.PrivateConstructorSerialisable;
import test.scenarium.river.modelTest.PrivateConstructorWithEditor;
import test.scenarium.river.modelTest.PrivateConstructorWithFileRecorder;
import test.scenarium.river.modelTest.PrivateConstructorWithStreamer;
import test.scenarium.river.modelTest.SerialisationMultipleVersion;

@TestMethodOrder(OrderAnnotation.class)
public class StreamFile {
	private static final int NB_RUN = 30;
	private static final int MAX_SIZE = 50;
	private static final int MAX_COUNT = 100;
	private static final Random RAND = new Random(System.currentTimeMillis());
	private static final Path TMP_FOLDER = getTemporaryFolder();
	// @formatter:off
	private static final Class<?>[] TESTED_TYPES = new Class<?>[] {
		boolean.class,
		byte.class,
		char.class,
		short.class,
		int.class,
		long.class,
		float.class,
		double.class,
		Boolean.class,
		Byte.class,
		Character.class,
		Short.class,
		Integer.class,
		Long.class,
		Float.class,
		Double.class,
		String.class,
		File.class,
		Color.class,
		Color4f.class,
		ControlType.class,
		InetSocketAddress.class,
		Point2i.class,
		Point2f.class,
		Point2d.class,
		Point3i.class,
		Point3f.class,
		Point3d.class,
		Point4i.class,
		Point4f.class,
		Point4d.class,
		Vector2f.class,
		Vector2d.class,
		Vector3f.class,
		Vector3d.class,
		Vector4f.class,
		Vector4d.class,
		Matrix3f.class,
		Matrix3d.class,
		Matrix4f.class,
		Matrix4d.class,
		LocalDate.class,
		LocalTime.class,
		LocalDateTime.class,
		// TODO pb with: GVector.class, wrong file in count
		BitSet.class,
		Path.class,
		// TODO pb with: GMatrix.class, wrong file in count
		// TODO pb with: Selection.class
	};
	// @formatter:on

	public StreamFile() {
		ModuleManager.birth();
	}

	private static String randomString(int count) {
		String s = new String();
		int nbChar = count;
		for (int i = 0; i < nbChar; i++) {
			char c = (char) RAND.nextInt();
			while ((c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c < '0' || c > '9'))
				c = (char) RAND.nextInt();
			s = s + c;
		}
		return s;
	}

	private static Path getTemporaryFolder() {
		String strTmp = System.getProperty("java.io.tmpdir");
		Path out = Paths.get(strTmp + FileSystems.getDefault().getSeparator() + "scRiverTest" + FileSystems.getDefault().getSeparator() + randomString(25));
		try {
			createFolder(out);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return out;
	}

	private static void createFolder(Path path) throws IOException {
		if (!Files.exists(path)) {
			Log.print("Create folder: " + path);
			Files.createDirectories(path);
		}
	}

	@Before
	public void initSerializer() {
		DataStreamManager.purgeStream(FixeSizeClass.class.getModule());
		FileStreamManager.purgeStream(FixeSizeClass.class.getModule());
		PropertyEditorManager.purgeEditors(FixeSizeClass.class.getModule());
		// force initialization of bean-editor ("ant" module not full supported right now)
		PropertyEditorManager.initilaizeEditors();
		//Log.print("add streamer & editor");
		DataStreamConsumer consumer = new DataStreamConsumer();
		consumer.accept(SerialisationMultipleVersion.class, SerialisationMultipleVersionInputStream.class, SerialisationMultipleVersionOutputStream.class);
		consumer.accept(FullConstructorWithStreamer.class, FullConstructorWithStreamerInputStream.class, FullConstructorWithStreamerOutputStream.class);
		consumer.accept(OpenClassWithStreamer.class, OpenClassWithStreamerInputStream.class, OpenClassWithStreamerOutputStream.class);
		consumer.accept(PrivateConstructorWithStreamer.class, PrivateConstructorWithStreamerInputStream.class, PrivateConstructorWithStreamerOutputStream.class);
		EditorConsumer consumerEditor = new EditorConsumer();
		consumerEditor.accept(FullConstructorWithEditor.class, FullConstructorWithEditorEditor.class);
		consumerEditor.accept(OpenClassWithEditor.class, OpenClassWithEditorEditor.class);
		consumerEditor.accept(PrivateConstructorWithEditor.class, PrivateConstructorWithEditorEditor.class);
		FileStreamConsumer consumerFile = new FileStreamConsumer();
		consumerFile.accept(FullConstructorWithFileRecorder.class, "FullConstructorWithFileRecorder", FullConstructorWithFileRecorderReader.class, FullConstructorWithFileRecorderRecorder.class);
		consumerFile.accept(OpenClassWithFileRecorder.class, "OpenClassWithFileRecorder", OpenClassWithFileRecorderReader.class, OpenClassWithFileRecorderRecorder.class);
		consumerFile.accept(PrivateConstructorWithFileRecorder.class, "PrivateConstructorWithFileRecorder", PrivateConstructorWithFileRecorderReader.class,
				PrivateConstructorWithFileRecorderRecorder.class);
	}

	@After
	public void uninitSerializer() {
		System.gc();
	}

	@Test
	@Order(1)
	public void testType() {
		int seed = (int) System.currentTimeMillis();
		Log.print("Test with seed: " + seed);
		for (int iii = 0; iii < TESTED_TYPES.length; iii++) {
			Class<?> type = TESTED_TYPES[iii];
			// force the seed fore every test to permit to reproduce an error if catch on build-farm.
			Random rand = new Random(seed);
			for (int k = 0; k < NB_RUN; k++) {
				Log.print("[" + (iii + 1) + " / " + TESTED_TYPES.length + "] Run test " + (k + 1) + " / " + NB_RUN + "    : " + type.getCanonicalName());
				testSimpleFileRecord(rand.nextLong(), MAX_COUNT, type, () -> ValueGenerator.generateRandomValue(type, rand.nextLong(), MAX_SIZE, TESTED_TYPES));
			}
		}
	}

	public void testSimpleFileRecord(long localSeed, int maxCount, Class<?> testType, Supplier<?> supplier) {
		Random rand = new Random(localSeed);

		// check if it exist
		assertTrue(FileStreamManager.hasStreamRecorder(testType));

		final FileStreamRecorder output = FileStreamManager.getRecorder(testType);
		assertNotNull(output);
		output.setPath(TMP_FOLDER.toString());
		assertEquals(output.getPath(), TMP_FOLDER.toString());
		output.setName("testFile");
		assertEquals(output.getName(), "testFile");
		String fullTestPath = output.getPath() + File.separator + output.getName() + "." + output.getExtention();

		// Create a list of object and serialize each.
		List<Object> reference = new ArrayList<>();
		int totalCount = rand.nextInt(maxCount - 10) + 10;
		for (int iii = 0; iii < totalCount; iii++) {
			assertEquals(iii, output.getPosition());
			Object tmpData = supplier.get();
			reference.add(tmpData);
			assertDoesNotThrow(() -> {
				output.push(tmpData);
			});
			assertEquals(iii + 1, output.getPosition());
		}
		// Close the output:
		assertDoesNotThrow(() -> {
			output.close();
		});

		// Create input stream to retrieve deserialized data
		assertDoesNotThrow(() -> {
			FileStreamManager.getRecordFileClass(new File(fullTestPath));
		});
		Class<?> detectedType = null;
		try {
			detectedType = FileStreamManager.getRecordFileClass(new File(fullTestPath));
			Class<?> checkWith = testType;
			if (checkWith == boolean.class)
				checkWith = Boolean.class;
			else if (checkWith == byte.class)
				checkWith = Byte.class;
			else if (checkWith == char.class)
				checkWith = Character.class;
			else if (checkWith == short.class)
				checkWith = Short.class;
			else if (checkWith == int.class)
				checkWith = Integer.class;
			else if (checkWith == long.class)
				checkWith = Long.class;
			else if (checkWith == float.class)
				checkWith = Float.class;
			else if (checkWith == double.class)
				checkWith = Double.class;
			if (checkWith.isAssignableFrom(detectedType))
				Log.warning("check multiple asingation error: " + detectedType.getCanonicalName() + " with reference: " + checkWith.getCanonicalName());
			else
				assertEquals(detectedType, checkWith);
		} catch (Exception ex) {
			// nothing to do...
			assertNotNull(null);
		}

		// check if it exist
		assertTrue(FileStreamManager.hasStreamReader(detectedType));

		final FileStreamReader input = FileStreamManager.getReader(detectedType);
		assertNotNull(input);
		input.setPath(TMP_FOLDER.toString());
		assertEquals(TMP_FOLDER.toString(), input.getPath());
		input.setName("testFile");
		assertEquals("testFile", input.getName());

		Log.error("in data " + input.size() + " / " + totalCount);
		assertEquals(input.size(), totalCount);

		List<Object> referenceInput = new ArrayList<>();
		for (int iii = 0; iii < totalCount; iii++) {
			assertEquals(iii, input.getPosition());
			assertDoesNotThrow(() -> {
				Object tmpData = input.pop();
				referenceInput.add(tmpData);
			});
			assertEquals(iii + 1, input.getPosition());
		}

		for (int iii = 0; iii < totalCount; iii++)
			assertTrue(reference.get(iii).equals(referenceInput.get(iii)));

		// Test random access:

		assertDoesNotThrow(() -> {
			input.seek(2);
			assertEquals(2, input.getPosition());
			Object tmpData2 = input.pop();
			assertEquals(3, input.getPosition());
			assertTrue(reference.get(2).equals(tmpData2));
		});

		assertDoesNotThrow(() -> {
			input.seek(8);
			assertEquals(8, input.getPosition());
			Object tmpData2 = input.pop();
			assertEquals(9, input.getPosition());
			assertTrue(reference.get(8).equals(tmpData2));
		});
		// Close the output:
		assertDoesNotThrow(() -> {
			input.close();
		});

	}

	@Test
	@Order(2)
	public void testSerializerVersion() {
		int seed = (int) System.currentTimeMillis();
		Log.print("Test with seed: " + seed);
		SerialisationMultipleVersionOutputStream.forceVersionOne = true;
		Class<?> type = SerialisationMultipleVersion.class;
		// force the seed fore every test to permit to reproduce an error if catch on build-farm.
		Random rand = new Random(seed);
		for (int k = 0; k < NB_RUN; k++) {
			Log.print("Run test " + (k + 1) + " / " + NB_RUN + "    : " + type.getCanonicalName());
			testSimpleFileRecord(rand.nextLong(), MAX_COUNT, type, () -> ValueGenerator.generateRandomValue(type, rand.nextLong(), MAX_SIZE, TESTED_TYPES));
		}
	}

	public void testType(Class<?> type) {
		SerialisationMultipleVersionOutputStream.forceVersionOne = false;
		int seed = (int) System.currentTimeMillis();
		Log.print("Test with seed: " + seed);
		Random rand = new Random(seed);
		for (int k = 0; k < NB_RUN; k++) {
			Log.print("Run test " + (k + 1) + " / " + NB_RUN + "    : " + type.getCanonicalName());
			testSimpleFileRecord(rand.nextLong(), MAX_COUNT, type, () -> ValueGenerator.generateRandomValue(type, rand.nextLong(), MAX_SIZE, TESTED_TYPES));
		}
	}

	public void testNotRecordableType(Class<?> testType) {
		// Check if type is recordable in output
		FileStreamRecorder output = FileStreamManager.getRecorder(testType);
		assertNull(output);
		// check if it exist
		assertFalse(FileStreamManager.hasStreamRecorder(testType));

		// Check if type is recordable in Input
		FileStreamReader input = FileStreamManager.getReader(testType);
		assertNull(input);
		// check if it exist
		assertFalse(FileStreamManager.hasStreamReader(testType));
	}

	@Test
	@Order(3)
	public void testOpenClass() {
		testNotRecordableType(OpenClass.class);
	}

	@Test
	@Order(4)
	public void testOpenClassSerialisable() {
		testType(OpenClassSerialisable.class);
	}

	@Test
	@Order(5)
	public void testOpenClassWithEditor() {
		testType(OpenClassWithEditor.class);
	}

	@Test
	@Order(6)
	public void testOpenClassWithFileRecorder() {
		testType(OpenClassWithFileRecorder.class);
	}

	@Test
	@Order(7)
	public void testOpenClassWithStreamer() {
		testType(OpenClassWithStreamer.class);
	}

	@Test
	@Order(8)
	public void testPrivateConstructor() {
		testNotRecordableType(PrivateConstructor.class);
	}

	@Test
	@Order(9)
	public void testPrivateConstructorSerialisable() {
		testType(PrivateConstructorSerialisable.class);
	}

	@Test
	@Order(10)
	public void testPrivateConstructorWithEditor() {
		testType(PrivateConstructorWithEditor.class);
	}

	@Test
	@Order(11)
	public void testPrivateConstructorWithFileRecorder() {
		testType(PrivateConstructorWithFileRecorder.class);
	}

	@Test
	@Order(12)
	public void testPrivateConstructorWithStreamer() {
		testType(PrivateConstructorWithStreamer.class);
	}

	@Test
	@Order(13)
	public void testFullConstructor() {
		testNotRecordableType(FullConstructor.class);
	}

	@Test
	@Order(14)
	public void testFullConstructorSerialisable() {
		testType(FullConstructorSerialisable.class);
	}

	@Test
	@Order(15)
	public void testFullConstructorWithEditor() {
		testType(FullConstructorWithEditor.class);
	}

	@Test
	@Order(16)
	public void testFullConstructorWithFileRecorder() {
		testType(FullConstructorWithFileRecorder.class);
	}

	@Test
	@Order(17)
	public void testFullConstructorWithStreamer() {
		testType(FullConstructorWithStreamer.class);
	}

	@Test
	@Order(18)
	public void testOrderSerializerType() {
		Class<?> testType = FixeSizeClass.class;
		// [1] no add serializable model ==> must be serializable wit the Serialization of java Object
		{
			// check if it exist
			assertTrue(FileStreamManager.hasStreamRecorder(testType));
			// Check if type is recordable in output
			FileStreamRecorder output = FileStreamManager.getRecorder(testType);
			assertNotNull(output);
			assertTrue(output instanceof SerializableObjectStreamRecorder);
			// check if it exist
			assertTrue(FileStreamManager.hasStreamReader(testType));
			// Check if type is recordable in Input
			FileStreamReader input = FileStreamManager.getReader(testType);
			assertNotNull(input);
			assertTrue(input instanceof SerializableObjectStreamReader);
		}
		// [2] Test EDITOR
		{
			EditorConsumer consumerEditor = new EditorConsumer();
			consumerEditor.accept(FixeSizeClass.class, FixeSizeClassEditor.class);
			// check if it exist
			assertTrue(FileStreamManager.hasStreamRecorder(testType));
			// Check if type is recordable in output
			FileStreamRecorder output = FileStreamManager.getRecorder(testType);
			assertNotNull(output);
			assertTrue(output instanceof PropertyStreamRecorder);
			// check if it exist
			assertTrue(FileStreamManager.hasStreamReader(testType));
			// Check if type is recordable in Input
			FileStreamReader input = FileStreamManager.getReader(testType);
			assertNotNull(input);
			assertTrue(input instanceof PropertyStreamReader);
		}
		// [3] Test Streamer
		{
			DataStreamConsumer consumer = new DataStreamConsumer();
			consumer.accept(FixeSizeClass.class, FixeSizeClassInputStream.class, FixeSizeClassOutputStream.class);
			// check if it exist
			assertTrue(FileStreamManager.hasStreamRecorder(testType));
			// Check if type is recordable in output
			FileStreamRecorder output = FileStreamManager.getRecorder(testType);
			assertNotNull(output);
			assertTrue(output instanceof StreamableObjectStreamRecorder);
			// check if it exist
			assertTrue(FileStreamManager.hasStreamReader(testType));
			// Check if type is recordable in Input
			FileStreamReader input = FileStreamManager.getReader(testType);
			assertNotNull(input);
			assertTrue(input instanceof StreamableObjectStreamReader);
		}
		// [4] test file Streamer
		{
			FileStreamConsumer consumerFile = new FileStreamConsumer();
			consumerFile.accept(FixeSizeClass.class, FixeSizeClassReader.EXTENTION, FixeSizeClassReader.class, FixeSizeClassRecorder.class);
			// check if it exist
			assertTrue(FileStreamManager.hasStreamRecorder(testType));
			// Check if type is recordable in output
			FileStreamRecorder output = FileStreamManager.getRecorder(testType);
			assertNotNull(output);
			assertFalse(output instanceof PropertyStreamRecorder);
			assertFalse(output instanceof StreamableObjectStreamRecorder);
			assertFalse(output instanceof StreamableObjectStreamRecorder);
			// check if it exist
			assertTrue(FileStreamManager.hasStreamReader(testType));
			// Check if type is recordable in Input
			FileStreamReader input = FileStreamManager.getReader(testType);
			assertNotNull(input);
			assertFalse(input instanceof PropertyStreamReader);
			assertFalse(input instanceof StreamableObjectStreamReader);
			assertFalse(input instanceof StreamableObjectStreamReader);
		}
		// Remove section:
		{
			FileStreamRecorder output = null;
			FileStreamReader input = null;
			// Remove streamer
			FileStreamManager.unregisterStream(FixeSizeClass.class);
			output = FileStreamManager.getRecorder(testType);
			assertTrue(output instanceof StreamableObjectStreamRecorder);
			input = FileStreamManager.getReader(testType);
			assertTrue(input instanceof StreamableObjectStreamReader);
			// Remove streamer
			DataStreamManager.unregisterStream(FixeSizeClass.class);
			output = FileStreamManager.getRecorder(testType);
			assertTrue(output instanceof PropertyStreamRecorder);
			input = FileStreamManager.getReader(testType);
			assertTrue(input instanceof PropertyStreamReader);
			// Remove Editor:
			PropertyEditorManager.unregisterEditor(FixeSizeClass.class);
			output = FileStreamManager.getRecorder(testType);
			assertTrue(output instanceof SerializableObjectStreamRecorder);
			input = FileStreamManager.getReader(testType);
			assertTrue(input instanceof SerializableObjectStreamReader);
		}
	}

	@Test
	@Order(19)
	public void testPatialAddFileStreamer() {
		Class<?> testType = FixeSizeClass.class;
		{
			FileStreamRecorder output = null;
			FileStreamReader input = null;
			// Remove streamer
			FileStreamManager.unregisterStream(testType);

			output = FileStreamManager.getRecorder(testType);
			assertTrue(output instanceof SerializableObjectStreamRecorder);
			input = FileStreamManager.getReader(testType);
			assertTrue(input instanceof SerializableObjectStreamReader);

			FileStreamConsumer consumerFile = new FileStreamConsumer();
			// add writer only
			consumerFile.accept(FixeSizeClass.class, FixeSizeClassReader.EXTENTION, null, FixeSizeClassRecorder.class);
			output = FileStreamManager.getRecorder(testType);
			assertFalse(output instanceof PropertyStreamRecorder);
			assertFalse(output instanceof StreamableObjectStreamRecorder);
			assertFalse(output instanceof StreamableObjectStreamRecorder);
			input = FileStreamManager.getReader(testType);
			assertTrue(input instanceof SerializableObjectStreamReader);

			FileStreamManager.unregisterStream(FixeSizeClass.class);
			output = FileStreamManager.getRecorder(testType);
			assertTrue(output instanceof SerializableObjectStreamRecorder);
			input = FileStreamManager.getReader(testType);
			assertTrue(input instanceof SerializableObjectStreamReader);

			// add reader only
			consumerFile.accept(FixeSizeClass.class, FixeSizeClassReader.EXTENTION, FixeSizeClassReader.class, null);
			output = FileStreamManager.getRecorder(testType);
			assertTrue(output instanceof SerializableObjectStreamRecorder);
			input = FileStreamManager.getReader(testType);
			assertFalse(input instanceof PropertyStreamReader);
			assertFalse(input instanceof StreamableObjectStreamReader);
			assertFalse(input instanceof StreamableObjectStreamReader);

			FileStreamManager.unregisterStream(FixeSizeClass.class);
			output = FileStreamManager.getRecorder(testType);
			assertTrue(output instanceof SerializableObjectStreamRecorder);
			input = FileStreamManager.getReader(testType);
			assertTrue(input instanceof SerializableObjectStreamReader);
		}
		// Check assert if no streamer:
		{
			assertThrows(IllegalArgumentException.class, () -> {
				FileStreamConsumer consumer = new FileStreamConsumer();
				consumer.accept(FixeSizeClass.class, "zerzer", null, null);
			});
			assertThrows(IllegalArgumentException.class, () -> {
				FileStreamConsumer consumer = new FileStreamConsumer();
				consumer.accept(null, FixeSizeClassReader.EXTENTION, FixeSizeClassReader.class, FixeSizeClassRecorder.class);
			});
			assertThrows(IllegalArgumentException.class, () -> {
				FileStreamConsumer consumer = new FileStreamConsumer();
				consumer.accept(FixeSizeClass.class, null, FixeSizeClassReader.class, FixeSizeClassRecorder.class);
			});

		}
	}

	@Test
	@Order(20)
	public void testRemoveModule() {
		FileStreamRecorder output = null;
		FileStreamReader input = null;
		Class<?> testType = FixeSizeClass.class;
		FileStreamConsumer consumer = new FileStreamConsumer();
		consumer.accept(FixeSizeClass.class, FixeSizeClassReader.EXTENTION, FixeSizeClassReader.class, FixeSizeClassRecorder.class);
		output = FileStreamManager.getRecorder(testType);
		assertFalse(output instanceof PropertyStreamRecorder);
		assertFalse(output instanceof StreamableObjectStreamRecorder);
		assertFalse(output instanceof SerializableObjectStreamRecorder);
		input = FileStreamManager.getReader(testType);
		assertFalse(input instanceof PropertyStreamReader);
		assertFalse(input instanceof StreamableObjectStreamReader);
		assertFalse(input instanceof SerializableObjectStreamRecorder);
		// Remove streamer with module reference
		FileStreamManager.purgeStream(FixeSizeClass.class.getModule());
		output = FileStreamManager.getRecorder(testType);
		assertTrue(output instanceof SerializableObjectStreamRecorder);
		input = FileStreamManager.getReader(testType);
		assertTrue(input instanceof SerializableObjectStreamReader);
	}

	@Test
	@Order(21)
	public void checkFormatNames() {
		DataStreamManager.purgeStream(FixeSizeClass.class.getModule());
		FileStreamManager.purgeStream(FixeSizeClass.class.getModule());
		PropertyEditorManager.purgeEditors(FixeSizeClass.class.getModule());

		String[] tmp = FileStreamManager.getReaderFormatNames();
		assertEquals(tmp.length, 1);
		assertEquals(tmp[0], "ppd");
		FileStreamConsumer consumer = new FileStreamConsumer();
		consumer.accept(FixeSizeClass.class, FixeSizeClassReader.EXTENTION, FixeSizeClassReader.class, FixeSizeClassRecorder.class);
		tmp = FileStreamManager.getReaderFormatNames();
		assertEquals(tmp.length, 2);
		assertEquals(tmp[0], "ppd");
		assertEquals(tmp[1], FixeSizeClassReader.EXTENTION);

	}

}
