package test.scenarium.river.datastream.input;

import java.io.IOException;

import io.scenarium.river.datastream.input.GenericDataFlowInputStream;

import test.scenarium.river.modelTest.SerialisationMultipleVersion;

public class SerialisationMultipleVersionInputStream extends GenericDataFlowInputStream<SerialisationMultipleVersion> {
	@Override
	public SerialisationMultipleVersion pop() throws IOException {
		long versionId = this.dataInput.readShort();
		if (versionId == 2L) {
			// Read the Version 2 of the reader.
			SerialisationMultipleVersion value = new SerialisationMultipleVersion();
			value.testBoolean = this.dataInput.readBoolean();
			value.testByte = this.dataInput.readByte();
			value.testChar = this.dataInput.readChar();
			value.testShort = this.dataInput.readShort();
			value.testInt = this.dataInput.readInt();
			value.testLong = this.dataInput.readLong();
			value.testFloat = this.dataInput.readFloat();
			value.testDouble = this.dataInput.readDouble();
			value.testString = this.dataInput.readUTF();
			return value;
		} else if (versionId == 1L) {
			// Read the Version 1 of the reader.
			SerialisationMultipleVersion value = new SerialisationMultipleVersion();
			value.testBoolean = this.dataInput.readBoolean();
			value.testByte = this.dataInput.readByte();
			value.testShort = this.dataInput.readShort();
			value.testChar = this.dataInput.readChar();
			value.testInt = this.dataInput.readInt();
			value.testString = this.dataInput.readUTF();
			value.testLong = this.dataInput.readLong();
			value.testFloat = this.dataInput.readFloat();
			value.testDouble = this.dataInput.readDouble();
			return value;
		}
		throw new IOException("BaseSerializeTestClass deserialisation vertion not supported: decode=" + versionId + " require<=" + SerialisationMultipleVersion.serialVersionUID);
	}

}
