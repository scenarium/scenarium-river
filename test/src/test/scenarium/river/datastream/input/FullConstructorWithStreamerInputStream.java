package test.scenarium.river.datastream.input;

import java.io.IOException;

import io.scenarium.river.datastream.input.GenericDataFlowInputStream;

import test.scenarium.river.modelTest.FullConstructorWithStreamer;

public class FullConstructorWithStreamerInputStream extends GenericDataFlowInputStream<FullConstructorWithStreamer> {
	@Override
	public FullConstructorWithStreamer pop() throws IOException {
		long versionId = this.dataInput.readShort();
		if (versionId == 1L) {
			boolean testBoolean = this.dataInput.readBoolean();
			byte testByte = this.dataInput.readByte();
			char testChar = this.dataInput.readChar();
			short testShort = this.dataInput.readShort();
			int testInt = this.dataInput.readInt();
			long testLong = this.dataInput.readLong();
			float testFloat = this.dataInput.readFloat();
			double testDouble = this.dataInput.readDouble();
			String testString = this.dataInput.readUTF();
			return new FullConstructorWithStreamer(testBoolean, testByte, testChar, testShort, testInt, testLong, testFloat, testDouble, testString);
		}
		throw new IOException("FullConstructorWithStreamer deserialisation vertion not supported: decode=" + versionId + " require<=" + FullConstructorWithStreamer.serialVersionUID);
	}

}
