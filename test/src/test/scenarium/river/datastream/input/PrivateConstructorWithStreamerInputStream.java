package test.scenarium.river.datastream.input;

import java.io.IOException;

import io.scenarium.river.datastream.input.GenericDataFlowInputStream;

import test.scenarium.river.modelTest.PrivateConstructorWithStreamer;

public class PrivateConstructorWithStreamerInputStream extends GenericDataFlowInputStream<PrivateConstructorWithStreamer> {
	@Override
	public PrivateConstructorWithStreamer pop() throws IOException {
		long versionId = this.dataInput.readShort();
		if (versionId == 1L) {
			PrivateConstructorWithStreamer value = PrivateConstructorWithStreamer.create();
			value.testBoolean = this.dataInput.readBoolean();
			value.testByte = this.dataInput.readByte();
			value.testChar = this.dataInput.readChar();
			value.testShort = this.dataInput.readShort();
			value.testInt = this.dataInput.readInt();
			value.testLong = this.dataInput.readLong();
			value.testFloat = this.dataInput.readFloat();
			value.testDouble = this.dataInput.readDouble();
			value.testString = this.dataInput.readUTF();
			return value;
		}
		throw new IOException("PrivateConstructorWithStreamer deserialisation vertion not supported: decode=" + versionId + " require<=" + PrivateConstructorWithStreamer.serialVersionUID);
	}

}
