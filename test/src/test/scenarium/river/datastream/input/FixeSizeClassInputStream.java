package test.scenarium.river.datastream.input;

import java.io.IOException;

import io.scenarium.river.datastream.input.GenericDataFlowInputStream;

import test.scenarium.river.modelTest.FixeSizeClass;

public class FixeSizeClassInputStream extends GenericDataFlowInputStream<FixeSizeClass> {
	@Override
	public FixeSizeClass pop() throws IOException {
		long versionId = this.dataInput.readShort();
		if (versionId == 1L) {
			boolean testBoolean = this.dataInput.readBoolean();
			int testInt = this.dataInput.readInt();
			long testLong = this.dataInput.readLong();
			float testFloat = this.dataInput.readFloat();
			return new FixeSizeClass(testBoolean, testInt, testLong, testFloat);
		}
		throw new IOException("FixeSizeClass deserialisation vertion not supported: decode=" + versionId + " require<=" + FixeSizeClass.serialVersionUID);
	}

}
