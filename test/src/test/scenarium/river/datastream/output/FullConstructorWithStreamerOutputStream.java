package test.scenarium.river.datastream.output;

import java.io.IOException;

import io.scenarium.river.datastream.output.GenericDataFlowOutputStream;

import test.scenarium.river.modelTest.FullConstructorWithStreamer;

public class FullConstructorWithStreamerOutputStream extends GenericDataFlowOutputStream<FullConstructorWithStreamer> {

	public static boolean forceVersionOne = false;

	@Override
	public void push(FullConstructorWithStreamer value) throws IOException {
		this.dataOutput.writeShort((int) FullConstructorWithStreamer.serialVersionUID);
		this.dataOutput.writeBoolean(value.isTestBoolean());
		this.dataOutput.writeByte(value.getTestByte());
		this.dataOutput.writeChar(value.getTestChar());
		this.dataOutput.writeShort(value.getTestShort());
		this.dataOutput.writeInt(value.getTestInt());
		this.dataOutput.writeLong(value.getTestLong());
		this.dataOutput.writeFloat(value.getTestFloat());
		this.dataOutput.writeDouble(value.getTestDouble());
		this.dataOutput.writeUTF(value.getTestString());
	}
}
