package test.scenarium.river.datastream.output;

import java.io.IOException;

import io.scenarium.river.datastream.output.GenericDataFlowOutputStream;

import test.scenarium.river.modelTest.OpenClassWithStreamer;

public class OpenClassWithStreamerOutputStream extends GenericDataFlowOutputStream<OpenClassWithStreamer> {

	public static boolean forceVersionOne = false;

	@Override
	public void push(OpenClassWithStreamer value) throws IOException {
		this.dataOutput.writeShort((int) OpenClassWithStreamer.serialVersionUID);
		this.dataOutput.writeBoolean(value.testBoolean);
		this.dataOutput.writeByte(value.testByte);
		this.dataOutput.writeChar(value.testChar);
		this.dataOutput.writeShort(value.testShort);
		this.dataOutput.writeInt(value.testInt);
		this.dataOutput.writeLong(value.testLong);
		this.dataOutput.writeFloat(value.testFloat);
		this.dataOutput.writeDouble(value.testDouble);
		this.dataOutput.writeUTF(value.testString);
	}
}
