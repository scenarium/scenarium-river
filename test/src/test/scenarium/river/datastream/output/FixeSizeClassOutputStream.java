package test.scenarium.river.datastream.output;

import java.io.IOException;

import io.scenarium.river.datastream.output.GenericDataFlowOutputStream;

import test.scenarium.river.modelTest.FixeSizeClass;

public class FixeSizeClassOutputStream extends GenericDataFlowOutputStream<FixeSizeClass> {

	public static boolean forceVersionOne = false;

	@Override
	public void push(FixeSizeClass value) throws IOException {
		this.dataOutput.writeShort((short) FixeSizeClass.serialVersionUID);
		this.dataOutput.writeBoolean(value.isTestBoolean());
		this.dataOutput.writeInt(value.getTestInt());
		this.dataOutput.writeLong(value.getTestLong());
		this.dataOutput.writeFloat(value.getTestFloat());
	}
}
