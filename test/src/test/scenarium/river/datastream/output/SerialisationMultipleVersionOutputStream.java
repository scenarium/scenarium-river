package test.scenarium.river.datastream.output;

import java.io.IOException;

import io.scenarium.river.datastream.output.GenericDataFlowOutputStream;

import test.scenarium.river.modelTest.SerialisationMultipleVersion;

public class SerialisationMultipleVersionOutputStream extends GenericDataFlowOutputStream<SerialisationMultipleVersion> {

	public static boolean forceVersionOne = false;

	@Override
	public void push(SerialisationMultipleVersion value) throws IOException {
		if (forceVersionOne) {
			this.dataOutput.writeShort(1);
			this.dataOutput.writeBoolean(value.testBoolean);
			this.dataOutput.writeByte(value.testByte);
			this.dataOutput.writeShort(value.testShort);
			this.dataOutput.writeChar(value.testChar);
			this.dataOutput.writeInt(value.testInt);
			this.dataOutput.writeUTF(value.testString);
			this.dataOutput.writeLong(value.testLong);
			this.dataOutput.writeFloat(value.testFloat);
			this.dataOutput.writeDouble(value.testDouble);
		} else {
			this.dataOutput.writeShort((int) SerialisationMultipleVersion.serialVersionUID);
			this.dataOutput.writeBoolean(value.testBoolean);
			this.dataOutput.writeByte(value.testByte);
			this.dataOutput.writeChar(value.testChar);
			this.dataOutput.writeShort(value.testShort);
			this.dataOutput.writeInt(value.testInt);
			this.dataOutput.writeLong(value.testLong);
			this.dataOutput.writeFloat(value.testFloat);
			this.dataOutput.writeDouble(value.testDouble);
			this.dataOutput.writeUTF(value.testString);
		}
	}
}
