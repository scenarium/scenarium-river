/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package test.scenarium.river;

import java.util.ArrayList;
import java.util.List;

import io.scenarium.logger.Logger;
import io.scenarium.river.internal.Log;

import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
//import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(OrderAnnotation.class)
public class TestLog {

	@Test
	@Order(1)
	public void aaFirstInitialisation() {
		List<String> args = new ArrayList<>();
		args.add("--log-color");
		args.add("--log-lib=sc-log-test:verbose");
		Logger.init(args);
	}

	@Test
	@Order(2)
	public void ccBasicLogCall() {
		Log.print("Simple print");
		Log.todo("Simple todo");
		Log.error("Simple error");
		Log.warning("Simple warning");
		Log.info("Simple info");
		Log.debug("Simple debug");
		Log.verbose("Simple verbose");
	}

}
