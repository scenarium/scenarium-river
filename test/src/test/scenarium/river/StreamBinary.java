/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     DUPIN Edouard - initial API and implementation
 ******************************************************************************/
package test.scenarium.river;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.net.InetSocketAddress;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

import javax.vecmath.Color4f;
import javax.vecmath.GMatrix;
import javax.vecmath.GVector;
import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4d;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point2d;
import javax.vecmath.Point2f;
import javax.vecmath.Point2i;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Point3i;
import javax.vecmath.Point4d;
import javax.vecmath.Point4f;
import javax.vecmath.Point4i;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4d;
import javax.vecmath.Vector4f;

import io.beanmanager.consumer.EditorConsumer;
import io.beanmanager.editors.PropertyEditorManager;
import io.beanmanager.editors.primitive.number.ControlType;
import io.scenarium.pluginManager.ModuleManager;
import io.scenarium.river.consumer.DataStreamConsumer;
import io.scenarium.river.datastream.DataStreamManager;
import io.scenarium.river.datastream.input.DataFlowInputStream;
import io.scenarium.river.datastream.input.PropertyInputStream;
import io.scenarium.river.datastream.input.SerializableInputStream;
import io.scenarium.river.datastream.output.DataFlowOutputStream;
import io.scenarium.river.datastream.output.PropertyOutputStream;
import io.scenarium.river.datastream.output.SerializableOutputStream;
import io.scenarium.river.filerecorder.FileStreamManager;
import io.scenarium.river.internal.Log;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;

import javafx.scene.paint.Color;
import test.scenarium.river.datastream.input.FixeSizeClassInputStream;
import test.scenarium.river.datastream.input.FullConstructorWithStreamerInputStream;
import test.scenarium.river.datastream.input.OpenClassWithStreamerInputStream;
import test.scenarium.river.datastream.input.PrivateConstructorWithStreamerInputStream;
import test.scenarium.river.datastream.input.SerialisationMultipleVersionInputStream;
import test.scenarium.river.datastream.output.FixeSizeClassOutputStream;
import test.scenarium.river.datastream.output.FullConstructorWithStreamerOutputStream;
import test.scenarium.river.datastream.output.OpenClassWithStreamerOutputStream;
import test.scenarium.river.datastream.output.PrivateConstructorWithStreamerOutputStream;
import test.scenarium.river.datastream.output.SerialisationMultipleVersionOutputStream;
import test.scenarium.river.editor.FixeSizeClassEditor;
import test.scenarium.river.editor.FullConstructorWithEditorEditor;
import test.scenarium.river.editor.OpenClassWithEditorEditor;
import test.scenarium.river.editor.PrivateConstructorWithEditorEditor;
import test.scenarium.river.modelTest.FixeSizeClass;
import test.scenarium.river.modelTest.FullConstructor;
import test.scenarium.river.modelTest.FullConstructorSerialisable;
import test.scenarium.river.modelTest.FullConstructorWithEditor;
import test.scenarium.river.modelTest.FullConstructorWithFileRecorder;
import test.scenarium.river.modelTest.FullConstructorWithStreamer;
import test.scenarium.river.modelTest.OpenClass;
import test.scenarium.river.modelTest.OpenClassSerialisable;
import test.scenarium.river.modelTest.OpenClassWithEditor;
import test.scenarium.river.modelTest.OpenClassWithFileRecorder;
import test.scenarium.river.modelTest.OpenClassWithStreamer;
import test.scenarium.river.modelTest.PrivateConstructor;
import test.scenarium.river.modelTest.PrivateConstructorSerialisable;
import test.scenarium.river.modelTest.PrivateConstructorWithEditor;
import test.scenarium.river.modelTest.PrivateConstructorWithFileRecorder;
import test.scenarium.river.modelTest.PrivateConstructorWithStreamer;
import test.scenarium.river.modelTest.SerialisationMultipleVersion;

@TestMethodOrder(OrderAnnotation.class)
public class StreamBinary {
	private static final int NB_RUN = 30;
	private static final int MAX_SIZE = 50;
	private static final int MAX_COUNT = 100;
	// @formatter:off
	private static final Class<?>[] TESTED_TYPES = new Class<?>[] {
		boolean.class,
		byte.class,
		char.class,
		short.class,
		int.class,
		long.class,
		float.class,
		double.class,
		Boolean.class,
		Byte.class,
		Character.class,
		Short.class,
		Integer.class,
		Long.class,
		Float.class,
		Double.class,
		String.class,
		File.class,
		Color.class,
		Color4f.class,
		ControlType.class,
		InetSocketAddress.class,
		Point2i.class,
		Point2f.class,
		Point2d.class,
		Point3i.class,
		Point3f.class,
		Point3d.class,
		Point4i.class,
		Point4f.class,
		Point4d.class,
		Vector2f.class,
		Vector2d.class,
		Vector3f.class,
		Vector3d.class,
		Vector4f.class,
		Vector4d.class,
		Matrix3f.class,
		Matrix3d.class,
		Matrix4f.class,
		Matrix4d.class,
		LocalDate.class,
		LocalTime.class,
		LocalDateTime.class,
		GVector.class,
		BitSet.class,
		Path.class,
		GMatrix.class
		// TODO pb with: Selection.class
	};
	// @formatter:on

	public StreamBinary() {
		ModuleManager.birth();
	}

	@Before
	public void initSerializer() {
		// clean all test serialize
		DataStreamManager.purgeStream(FixeSizeClass.class.getModule());
		FileStreamManager.purgeStream(FixeSizeClass.class.getModule());
		PropertyEditorManager.purgeEditors(FixeSizeClass.class.getModule());
		// force initialization of bean-editor ("ant" module not full supported right now)
		PropertyEditorManager.initilaizeEditors();
		// add test serialize
		DataStreamConsumer consumer = new DataStreamConsumer();
		consumer.accept(SerialisationMultipleVersion.class, SerialisationMultipleVersionInputStream.class, SerialisationMultipleVersionOutputStream.class);
		consumer.accept(FullConstructorWithStreamer.class, FullConstructorWithStreamerInputStream.class, FullConstructorWithStreamerOutputStream.class);
		consumer.accept(OpenClassWithStreamer.class, OpenClassWithStreamerInputStream.class, OpenClassWithStreamerOutputStream.class);
		consumer.accept(PrivateConstructorWithStreamer.class, PrivateConstructorWithStreamerInputStream.class, PrivateConstructorWithStreamerOutputStream.class);
		EditorConsumer consumerEditor = new EditorConsumer();
		consumerEditor.accept(FullConstructorWithEditor.class, FullConstructorWithEditorEditor.class);
		consumerEditor.accept(OpenClassWithEditor.class, OpenClassWithEditorEditor.class);
		consumerEditor.accept(PrivateConstructorWithEditor.class, PrivateConstructorWithEditorEditor.class);
	}

	@After
	public void uninitSerializer() {
		System.gc();
	}

	@Test
	@Order(1)
	public void testTypeGeneric() {
		//Class<?> type = OpenClass.class;
		SerialisationMultipleVersionOutputStream.forceVersionOne = false;
		int seed = (int) System.currentTimeMillis();
		Log.print("Test with seed: " + seed);
		for (int iii = 0; iii < TESTED_TYPES.length; iii++) {
			Class<?> type = TESTED_TYPES[iii];
			// force the seed fore every test to permit to reproduce an error if catch on build-farm.
			Random rand = new Random(seed);
			for (int k = 0; k < NB_RUN; k++) {
				Log.print("[" + (iii + 1) + " / " + TESTED_TYPES.length + "] Run test " + (k + 1) + " / " + NB_RUN + "    : " + type.getCanonicalName());
				testSimpleStream(rand.nextLong(), MAX_COUNT, type, () -> ValueGenerator.generateRandomValue(type, rand.nextLong(), MAX_SIZE, TESTED_TYPES));
			}
		}
	}

	public void testSimpleStream(long localSeed, int maxCount, Class<?> testType, Supplier<?> supplier) {
		Random rand = new Random(localSeed);
		Log.print("test: " + testType.getCanonicalName());
		// check if exist:
		assertTrue(DataStreamManager.hasOutputStream(testType));

		// Create output stream to generate serialization of the data
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		final DataFlowOutputStream<?> output = DataStreamManager.createOutputStream(testType);
		assertNotNull(output);
		output.setDataOutput(dos);

		// Create a list of object and serialize each.
		List<Object> reference = new ArrayList<>();
		int totalCount = rand.nextInt(maxCount - 10) + 10;
		for (int iii = 0; iii < totalCount; iii++) {
			Object tmpData = supplier.get();
			reference.add(tmpData);
			assertDoesNotThrow(() -> {
				output.pushObject(tmpData);
			});
		}

		// check if exist:
		assertTrue(DataStreamManager.hasInputStream(testType));

		// Create input stream to retrieve deserialized data
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(baos.toByteArray()));
		final DataFlowInputStream<?> input = DataStreamManager.createInputStream(testType);
		assertNotNull(input);
		input.setDataInput(dis);
		List<Object> referenceInput = new ArrayList<>();
		for (int iii = 0; iii < totalCount; iii++)
			assertDoesNotThrow(() -> {
				Object tmpData;
				tmpData = input.pop();
				referenceInput.add(tmpData);
			});

		for (int iii = 0; iii < totalCount; iii++)
			assertTrue(reference.get(iii).equals(referenceInput.get(iii)));
	}

	@Test
	@Order(2)
	public void testSerializerVersion() {
		int seed = (int) System.currentTimeMillis();
		Log.print("Test with seed: " + seed);
		SerialisationMultipleVersionOutputStream.forceVersionOne = true;
		Class<?> type = SerialisationMultipleVersion.class;
		// force the seed fore every test to permit to reproduce an error if catch on build-farm.
		Random rand = new Random(seed);
		for (int k = 0; k < NB_RUN; k++) {
			Log.print("Run test " + (k + 1) + " / " + NB_RUN + "    : " + type.getCanonicalName());
			testSimpleStream(rand.nextLong(), MAX_COUNT, type, () -> ValueGenerator.generateRandomValue(type, rand.nextLong(), MAX_SIZE, TESTED_TYPES));
		}
	}

	public void testType(Class<?> type) {
		SerialisationMultipleVersionOutputStream.forceVersionOne = false;
		int seed = (int) System.currentTimeMillis();
		Log.print("Test with seed: " + seed);
		Random rand = new Random(seed);
		for (int k = 0; k < NB_RUN; k++) {
			Log.print("Run test " + (k + 1) + " / " + NB_RUN + "    : " + type.getCanonicalName());
			testSimpleStream(rand.nextLong(), MAX_COUNT, type, () -> ValueGenerator.generateRandomValue(type, rand.nextLong(), MAX_SIZE, TESTED_TYPES));
		}
	}

	public void testNotStreamableType(Class<?> testType) {
		// Check if type is serializable in output
		final DataFlowOutputStream<?> output = DataStreamManager.createOutputStream(testType);
		assertNull(output);
		// check if exist:
		assertFalse(DataStreamManager.hasOutputStream(testType));

		// Check if type is serializable in Input
		final DataFlowInputStream<?> input = DataStreamManager.createInputStream(testType);
		assertNull(input);
		// check if exist:
		assertFalse(DataStreamManager.hasInputStream(testType));
	}

	@Test
	@Order(3)
	public void testOpenClass() {
		testNotStreamableType(OpenClass.class);
	}

	@Test
	@Order(4)
	public void testOpenClassSerialisable() {
		testType(OpenClassSerialisable.class);
	}

	@Test
	@Order(4)
	public void testOpenClassWithEditor() {
		testType(OpenClassWithEditor.class);
	}

	@Test
	@Order(5)
	public void testOpenClassWithFileRecorder() {
		testNotStreamableType(OpenClassWithFileRecorder.class);
	}

	@Test
	@Order(7)
	public void testOpenClassWithStreamer() {
		testType(OpenClassWithStreamer.class);
	}

	@Test
	@Order(8)
	public void testPrivateConstructor() {
		testNotStreamableType(PrivateConstructor.class);
	}

	@Test
	@Order(9)
	public void testPrivateConstructorSerialisable() {
		testType(PrivateConstructorSerialisable.class);
	}

	@Test
	@Order(10)
	public void testPrivateConstructorWithEditor() {
		testType(PrivateConstructorWithEditor.class);
	}

	@Test
	@Order(11)
	public void testPrivateConstructorWithFileRecorder() {
		testNotStreamableType(PrivateConstructorWithFileRecorder.class);
	}

	@Test
	@Order(12)
	public void testPrivateConstructorWithStreamer() {
		testType(PrivateConstructorWithStreamer.class);
	}

	@Test
	@Order(13)
	public void testFullConstructor() {
		testNotStreamableType(FullConstructor.class);
	}

	@Test
	@Order(14)
	public void testFullConstructorSerialisable() {
		testType(FullConstructorSerialisable.class);
	}

	@Test
	@Order(15)
	public void testFullConstructorWithEditor() {
		testType(FullConstructorWithEditor.class);
	}

	@Test
	@Order(16)
	public void testFullConstructorWithFileRecorder() {
		testNotStreamableType(FullConstructorWithFileRecorder.class);
	}

	@Test
	@Order(17)
	public void testFullConstructorWithStreamer() {
		testType(FullConstructorWithStreamer.class);
	}

	@Test
	@Order(18)
	public void testOrderSerializerType() {
		Class<?> testType = FixeSizeClass.class;
		// [1] no add serializable model ==> must be serializable wit the Serialization of java Object
		{
			// check if exist:
			assertTrue(DataStreamManager.hasOutputStream(testType));
			// Get streamer output
			final DataFlowOutputStream<?> output = DataStreamManager.createOutputStream(testType);
			assertNotNull(output);
			assertTrue(output instanceof SerializableOutputStream);
			// check if exist:
			assertTrue(DataStreamManager.hasInputStream(testType));
			// Get streamer input
			final DataFlowInputStream<?> input = DataStreamManager.createInputStream(testType);
			assertNotNull(input);
			assertTrue(input instanceof SerializableInputStream);
		}
		// [2] Test EDITOR
		{
			EditorConsumer consumerEditor = new EditorConsumer();
			consumerEditor.accept(FixeSizeClass.class, FixeSizeClassEditor.class);
			// check if exist:
			assertTrue(DataStreamManager.hasOutputStream(testType));
			// Get streamer output
			final DataFlowOutputStream<?> output = DataStreamManager.createOutputStream(testType);
			assertNotNull(output);
			assertTrue(output instanceof PropertyOutputStream);
			// check if exist:
			assertTrue(DataStreamManager.hasInputStream(testType));
			// Get streamer input
			final DataFlowInputStream<?> input = DataStreamManager.createInputStream(testType);
			assertNotNull(input);
			assertTrue(input instanceof PropertyInputStream);
		}
		assertFalse(DataStreamManager.hasIntputStreamOnly(testType));
		// [3] Test Streamer
		{
			DataStreamConsumer consumer = new DataStreamConsumer();
			consumer.accept(FixeSizeClass.class, FixeSizeClassInputStream.class, FixeSizeClassOutputStream.class);
			// check if exist:
			assertTrue(DataStreamManager.hasOutputStream(testType));
			// Ge
			final DataFlowOutputStream<?> output = DataStreamManager.createOutputStream(testType);
			assertNotNull(output);
			assertFalse(output instanceof PropertyOutputStream);
			assertFalse(output instanceof SerializableOutputStream);
			// check if exist:
			assertTrue(DataStreamManager.hasInputStream(testType));
			// Get streamer input
			final DataFlowInputStream<?> input = DataStreamManager.createInputStream(testType);
			assertNotNull(input);
			assertFalse(input instanceof PropertyInputStream);
			assertFalse(input instanceof SerializableInputStream);
		}
		assertTrue(DataStreamManager.hasIntputStreamOnly(testType));

		// Remove section:
		{
			DataFlowOutputStream<?> output = null;
			DataFlowInputStream<?> input = null;
			// Remove streamer
			DataStreamManager.unregisterStream(FixeSizeClass.class);

			output = DataStreamManager.createOutputStream(testType);
			assertTrue(output instanceof PropertyOutputStream);
			input = DataStreamManager.createInputStream(testType);
			assertTrue(input instanceof PropertyInputStream);
			// Remove Editor:
			PropertyEditorManager.unregisterEditor(FixeSizeClass.class);
			output = DataStreamManager.createOutputStream(testType);
			assertTrue(output instanceof SerializableOutputStream);
			input = DataStreamManager.createInputStream(testType);
			assertTrue(input instanceof SerializableInputStream);
		}
	}

	@Test
	@Order(19)
	public void testPatialAddStreamer() {
		Class<?> testType = FixeSizeClass.class;
		{
			DataStreamConsumer consumer = new DataStreamConsumer();
			DataFlowOutputStream<?> output = null;
			DataFlowInputStream<?> input = null;
			// Remove streamer
			DataStreamManager.unregisterStream(FixeSizeClass.class);
			output = DataStreamManager.createOutputStream(testType);
			assertTrue(output instanceof SerializableOutputStream);
			input = DataStreamManager.createInputStream(testType);
			assertTrue(input instanceof SerializableInputStream);
			// add only input
			consumer.accept(FixeSizeClass.class, FixeSizeClassInputStream.class, null);
			output = DataStreamManager.createOutputStream(testType);
			assertTrue(output instanceof SerializableOutputStream);
			input = DataStreamManager.createInputStream(testType);
			// is streamer ?
			assertFalse(input instanceof PropertyInputStream);
			assertFalse(input instanceof SerializableInputStream);
			// Remove streamer
			DataStreamManager.unregisterStream(FixeSizeClass.class);
			output = DataStreamManager.createOutputStream(testType);
			assertTrue(output instanceof SerializableOutputStream);
			input = DataStreamManager.createInputStream(testType);
			assertTrue(input instanceof SerializableInputStream);
			// add only output
			consumer.accept(FixeSizeClass.class, null, FixeSizeClassOutputStream.class);
			output = DataStreamManager.createOutputStream(testType);
			// is streamer ?
			assertFalse(output instanceof PropertyOutputStream);
			assertFalse(output instanceof SerializableOutputStream);
			input = DataStreamManager.createInputStream(testType);
			assertTrue(input instanceof SerializableInputStream);
			// Remove streamer
			DataStreamManager.unregisterStream(FixeSizeClass.class);
			output = DataStreamManager.createOutputStream(testType);
			assertTrue(output instanceof SerializableOutputStream);
			input = DataStreamManager.createInputStream(testType);
			assertTrue(input instanceof SerializableInputStream);
		}
		// Check assert if no streamer:
		{
			assertThrows(IllegalArgumentException.class, () -> {
				DataStreamConsumer consumer = new DataStreamConsumer();
				consumer.accept(FixeSizeClass.class, null, null);
			});
			assertThrows(IllegalArgumentException.class, () -> {
				DataStreamConsumer consumer = new DataStreamConsumer();
				consumer.accept(null, FixeSizeClassInputStream.class, FixeSizeClassOutputStream.class);
			});

		}
	}

	@Test
	@Order(20)
	public void testRemoveModule() {
		Class<?> testType = FixeSizeClass.class;
		{
			DataFlowOutputStream<?> output = null;
			DataFlowInputStream<?> input = null;
			DataStreamConsumer consumer = new DataStreamConsumer();
			consumer.accept(FixeSizeClass.class, FixeSizeClassInputStream.class, FixeSizeClassOutputStream.class);
			output = DataStreamManager.createOutputStream(testType);
			// is streamer ?
			assertFalse(output instanceof PropertyOutputStream);
			assertFalse(output instanceof SerializableOutputStream);
			input = DataStreamManager.createInputStream(testType);
			// is streamer ?
			assertFalse(input instanceof PropertyInputStream);
			assertFalse(input instanceof SerializableInputStream);
			// Remove streamer with module reference
			DataStreamManager.purgeStream(FixeSizeClass.class.getModule());
			output = DataStreamManager.createOutputStream(testType);
			assertTrue(output instanceof SerializableOutputStream);
			input = DataStreamManager.createInputStream(testType);
			assertTrue(input instanceof SerializableInputStream);
		}
	}

	// TODO Test object serializer does not leak after 100000 test ???

}
