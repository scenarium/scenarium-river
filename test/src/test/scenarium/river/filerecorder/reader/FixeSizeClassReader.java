/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package test.scenarium.river.filerecorder.reader;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;

import io.scenarium.river.filerecorder.reader.GenericFileStreamReader;

import test.scenarium.river.modelTest.FixeSizeClass;

public class FixeSizeClassReader extends GenericFileStreamReader {
	public static final String EXTENTION = "FixeSizeClassData";
	private FileInputStream fis;
	private DataInputStream dataDis;
	private long headerSize;
	private final long dataSize = 1 /* boolean */ + Integer.BYTES + Long.BYTES + Float.BYTES + Character.BYTES /* \n */;
	private FileChannel channel;

	public FixeSizeClassReader() {
		super(EXTENTION);
	}

	@Override
	public void close() throws IOException {
		if (this.fis != null) {
			this.fis.close();
			this.fis = null;
		}
	}

	protected void initStream() throws IOException {
		if (this.fis == null) {
			File file = getFile();
			calculateSize(file);
			file.getParentFile().mkdirs();
			this.fis = new FileInputStream(file);
			this.channel = this.fis.getChannel();
			// remove header
			this.channel.position(this.headerSize);
			this.dataDis = new DataInputStream(this.fis);
			// maybe do some check here...
		}
	}

	public void calculateSize(File file) throws IOException {
		try (RandomAccessFile rafData = new RandomAccessFile(file, "r")) {
			// header is the first line :
			rafData.readLine();
			rafData.readLine();
			rafData.readLine();
			this.headerSize = rafData.getFilePointer();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IOException("Can not get header of '" + file + "'");
		}
	}

	@Override
	public Object pop() throws IOException, FileNotFoundException {
		initStream();
		boolean testBoolean = this.dataDis.readBoolean();
		int testInt = this.dataDis.readInt();
		long testLong = this.dataDis.readLong();
		float testFloat = this.dataDis.readFloat();
		return new FixeSizeClass(testBoolean, testInt, testLong, testFloat);
	}

	@Override
	public long size() {
		try {
			initStream();
		} catch (IOException e) {
			e.printStackTrace();
			return 0;
		}
		// TODO If the file index is not present ==> create a new one with re-indexing...
		return (getFile().length() - this.headerSize) / this.dataSize;
	}

	@Override
	public void seek(long frame) throws IOException {
		initStream();
		if (this.channel != null)
			this.channel.position(this.headerSize + frame * this.dataSize);
	}

}
