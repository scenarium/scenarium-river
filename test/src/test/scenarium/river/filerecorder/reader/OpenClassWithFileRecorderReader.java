/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package test.scenarium.river.filerecorder.reader;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;

import io.scenarium.river.filerecorder.reader.GenericFileStreamReader;

import test.scenarium.river.modelTest.OpenClassWithFileRecorder;

public class OpenClassWithFileRecorderReader extends GenericFileStreamReader {
	private FileInputStream fis;
	private DataInputStream dataDis;
	private long headerSize;
	private final long dataSize = 1 /* boolean */ + Byte.BYTES + Character.BYTES + Short.BYTES + Integer.BYTES + Long.BYTES + Float.BYTES + Double.BYTES + Byte.BYTES /* \n */;
	private FileChannel channel;

	public OpenClassWithFileRecorderReader() {
		super("OpenClassWithFileRecorder");
	}

	@Override
	public void close() throws IOException {
		if (this.fis != null) {
			this.fis.close();
			this.fis = null;
			this.position = 0;
		}
	}

	protected void initStream() throws IOException {
		if (this.fis == null) {
			File file = getFile();
			calculateSize(file);
			this.fis = new FileInputStream(file);
			this.channel = this.fis.getChannel();
			// remove header
			this.channel.position(this.headerSize);
			this.dataDis = new DataInputStream(this.fis);
			// maybe do some check here...
			this.position = 0;
		}
	}

	public void calculateSize(File file) throws IOException {
		try (RandomAccessFile rafData = new RandomAccessFile(file, "r")) {
			// header is the first line :
			rafData.readLine();
			rafData.readLine();
			rafData.readLine();
			this.headerSize = rafData.getFilePointer();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IOException("Can not get header of '" + file + "'");
		}
	}

	@Override
	public Object pop() throws IOException, FileNotFoundException {
		initStream();
		OpenClassWithFileRecorder value = new OpenClassWithFileRecorder();
		value.testBoolean = this.dataDis.readBoolean();
		value.testByte = this.dataDis.readByte();
		value.testChar = this.dataDis.readChar();
		value.testShort = this.dataDis.readShort();
		value.testInt = this.dataDis.readInt();
		value.testLong = this.dataDis.readLong();
		value.testFloat = this.dataDis.readFloat();
		value.testDouble = this.dataDis.readDouble();
		this.dataDis.readByte(); // remove '\n' ...
		this.position++;
		return value;
	}

	@Override
	public long size() {
		try {
			initStream();
		} catch (IOException e) {
			e.printStackTrace();
			return 0;
		}
		// TODO If the file index is not present ==> create a new one with re-indexing...
		return (getFile().length() - this.headerSize) / this.dataSize;
	}

	@Override
	public void seek(long frame) throws IOException {
		initStream();
		if (this.channel != null)
			this.channel.position(this.headerSize + frame * this.dataSize);
		this.position = frame;
	}

}
