/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package test.scenarium.river.filerecorder.recorder;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import io.beanmanager.BeanManager;
import io.scenarium.river.filerecorder.recorder.GenericFileStreamRecorder;

import test.scenarium.river.modelTest.PrivateConstructorWithFileRecorder;

public class PrivateConstructorWithFileRecorderRecorder extends GenericFileStreamRecorder {
	private FileOutputStream fos;
	private DataOutputStream dataDos;

	public PrivateConstructorWithFileRecorderRecorder() {
		super("PrivateConstructorWithFileRecorder");
	}

	@Override
	public void close() throws IOException {
		if (this.fos != null) {
			this.fos.flush();
			this.fos.close();
			this.fos = null;
			this.position = 0;
		}
	}

	protected void initStream() throws IOException {
		if (this.fos == null) {
			File file = getFile();
			file.getParentFile().mkdirs();
			this.fos = new FileOutputStream(file);
			this.dataDos = new DataOutputStream(this.fos);
			this.dataDos.writeBytes(BeanManager.getDescriptorFromClass(PrivateConstructorWithFileRecorder.class) + System.lineSeparator());
			this.dataDos.writeBytes("v:1" + System.lineSeparator());
			this.dataDos.writeBytes("b,...,d" + System.lineSeparator());
			this.position = 0;
		}
	}

	@Override
	public void push(Object object) throws IOException {
		initStream();
		PrivateConstructorWithFileRecorder value = (PrivateConstructorWithFileRecorder) object;
		this.dataDos.writeBoolean(value.testBoolean);
		this.dataDos.writeByte(value.testByte);
		this.dataDos.writeChar(value.testChar);
		this.dataDos.writeShort(value.testShort);
		this.dataDos.writeInt(value.testInt);
		this.dataDos.writeLong(value.testLong);
		this.dataDos.writeFloat(value.testFloat);
		this.dataDos.writeDouble(value.testDouble);
		this.dataDos.writeBytes("\n");
		this.position++;
	}

}
