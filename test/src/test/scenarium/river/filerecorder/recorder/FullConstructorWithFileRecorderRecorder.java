/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package test.scenarium.river.filerecorder.recorder;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import io.beanmanager.BeanManager;
import io.scenarium.river.filerecorder.recorder.GenericFileStreamRecorder;

import test.scenarium.river.modelTest.FullConstructorWithFileRecorder;

public class FullConstructorWithFileRecorderRecorder extends GenericFileStreamRecorder {
	private FileOutputStream fos;
	private DataOutputStream dataDos;

	public FullConstructorWithFileRecorderRecorder() {
		// the . at the start is a part of the test
		super(".FullConstructorWithFileRecorder");
	}

	@Override
	public void close() throws IOException {
		if (this.fos != null) {
			this.fos.flush();
			this.fos.close();
			this.fos = null;
			this.position = 0;
		}
	}

	protected void initStream() throws IOException {
		if (this.fos == null) {
			File file = getFile();
			file.getParentFile().mkdirs();
			this.fos = new FileOutputStream(file);
			this.dataDos = new DataOutputStream(this.fos);
			this.dataDos.writeBytes(BeanManager.getDescriptorFromClass(FullConstructorWithFileRecorder.class) + System.lineSeparator());
			this.dataDos.writeBytes("v:1" + System.lineSeparator());
			this.dataDos.writeBytes("b,...,d" + System.lineSeparator());
			this.position = 0;
		}
	}

	@Override
	public void push(Object object) throws IOException {
		initStream();
		FullConstructorWithFileRecorder value = (FullConstructorWithFileRecorder) object;
		this.dataDos.writeBoolean(value.isTestBoolean());
		this.dataDos.writeByte(value.getTestByte());
		this.dataDos.writeChar(value.getTestChar());
		this.dataDos.writeShort(value.getTestShort());
		this.dataDos.writeInt(value.getTestInt());
		this.dataDos.writeLong(value.getTestLong());
		this.dataDos.writeFloat(value.getTestFloat());
		this.dataDos.writeDouble(value.getTestDouble());
		this.dataDos.writeBytes("\n");
		this.position++;
	}

}
