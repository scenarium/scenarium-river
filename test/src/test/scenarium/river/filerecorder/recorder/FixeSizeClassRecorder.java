/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package test.scenarium.river.filerecorder.recorder;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import io.beanmanager.BeanManager;
import io.scenarium.river.filerecorder.recorder.GenericFileStreamRecorder;

import test.scenarium.river.modelTest.FixeSizeClass;

public class FixeSizeClassRecorder extends GenericFileStreamRecorder {
	private FileOutputStream fos;
	private DataOutputStream dataDos;

	public FixeSizeClassRecorder() {
		super("FixeSizeClassData");
	}

	@Override
	public void close() throws IOException {
		if (this.fos != null) {
			this.fos.flush();
			this.fos.close();
			this.fos = null;
		}
	}

	protected void initStream() throws IOException {
		if (this.fos == null) {
			File file = getFile();
			file.getParentFile().mkdirs();
			this.fos = new FileOutputStream(file);
			this.dataDos = new DataOutputStream(this.fos);
			this.dataDos.writeBytes(BeanManager.getDescriptorFromClass(FixeSizeClass.class) + System.lineSeparator());
			this.dataDos.writeBytes("v:1" + System.lineSeparator());
			this.dataDos.writeBytes("b,i,l,f" + System.lineSeparator());
		}
	}

	@Override
	public void push(Object object) throws IOException {
		initStream();
		FixeSizeClass value = (FixeSizeClass) object;
		this.dataDos.writeBoolean(value.isTestBoolean());
		this.dataDos.writeInt(value.getTestInt());
		this.dataDos.writeLong(value.getTestLong());
		this.dataDos.writeFloat(value.getTestFloat());
		//this.dataDos.writeBytes("\n");
	}

}
