package test.scenarium.river;

import java.io.File;
import java.lang.reflect.Array;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Year;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Random;

import javax.vecmath.Color4f;
import javax.vecmath.GMatrix;
import javax.vecmath.GVector;
import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4d;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point2d;
import javax.vecmath.Point2f;
import javax.vecmath.Point2i;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Point3i;
import javax.vecmath.Point4d;
import javax.vecmath.Point4f;
import javax.vecmath.Point4i;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4d;
import javax.vecmath.Vector4f;

import io.beanmanager.editors.primitive.number.ControlType;
import io.beanmanager.struct.Selection;

import javafx.scene.paint.Color;
import test.scenarium.river.modelTest.BaseTestClass2;
import test.scenarium.river.modelTest.FixeSizeClass;
import test.scenarium.river.modelTest.FullConstructor;
import test.scenarium.river.modelTest.FullConstructorSerialisable;
import test.scenarium.river.modelTest.FullConstructorWithEditor;
import test.scenarium.river.modelTest.FullConstructorWithFileRecorder;
import test.scenarium.river.modelTest.FullConstructorWithStreamer;
import test.scenarium.river.modelTest.OpenClass;
import test.scenarium.river.modelTest.OpenClassSerialisable;
import test.scenarium.river.modelTest.OpenClassWithEditor;
import test.scenarium.river.modelTest.OpenClassWithFileRecorder;
import test.scenarium.river.modelTest.OpenClassWithStreamer;
import test.scenarium.river.modelTest.PrivateConstructor;
import test.scenarium.river.modelTest.PrivateConstructorSerialisable;
import test.scenarium.river.modelTest.PrivateConstructorWithEditor;
import test.scenarium.river.modelTest.PrivateConstructorWithFileRecorder;
import test.scenarium.river.modelTest.PrivateConstructorWithStreamer;
import test.scenarium.river.modelTest.SerialisationMultipleVersion;

public class ValueGenerator {
	private ValueGenerator() {}

	private static String randomString(Random rand, int count) {
		String s = new String();
		int nbChar = count;
		for (int i = 0; i < nbChar; i++) {
			char c = (char) rand.nextInt();
			while ((c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c < '0' || c > '9'))
				c = (char) rand.nextInt();
			s = s + c;
		}
		return s;
	}

	@SuppressWarnings("unchecked")
	public static Object generateRandomValue(Class<?> type, long localSeed, int maxSize, Class<?>[] testedTypes) {
		Random rand = new Random(localSeed);
		if (type.equals(boolean.class) || type.equals(Boolean.class))
			return rand.nextBoolean();
		else if (type.equals(byte.class) || type.equals(Byte.class))
			return (byte) rand.nextInt();
		else if (type.equals(char.class) || type.equals(Character.class)) {
			char value = (char) rand.nextInt();
			while (Character.getType(value) == Character.SURROGATE)
				value = (char) rand.nextInt();
			return value;
		} else if (type.equals(short.class) || type.equals(Short.class))
			return (short) rand.nextInt();
		else if (type.equals(int.class) || type.equals(Integer.class))
			return rand.nextInt();
		else if (type.equals(long.class) || type.equals(Long.class))
			return rand.nextLong();
		else if (type.equals(float.class) || type.equals(Float.class))
			return rand.nextFloat();
		else if (type.equals(double.class) || type.equals(Double.class))
			return rand.nextDouble();
		else if (type.equals(String.class)) {
			String s = new String();
			int nbChar = rand.nextInt(10);
			for (int i = 0; i < nbChar; i++) {
				char c = (char) rand.nextInt();
				while (Character.getType(c) == Character.SURROGATE || c == '\n' || c == '\r')
					c = (char) rand.nextInt();
				s = s + c;
			}
			return s;
		} else if (type.equals(File.class))
			return new File((String) generateRandomValue(String.class, rand.nextLong(), maxSize, testedTypes));
		else if (type.equals(Path.class))
			while (true)
				try {
					return Path.of((String) generateRandomValue(String.class, rand.nextLong(), maxSize, testedTypes));
				} catch (InvalidPathException e) {}
		else if (type.equals(Color.class))
			return new Color(rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble());
		else if (type.equals(Color4f.class))
			return new Color4f(rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
		else if (type.equals(ControlType.class))
			return ControlType.values()[rand.nextInt(ControlType.values().length)];
		else if (type.equals(InetSocketAddress.class)) {
			int randChoice = rand.nextInt(4);
			if (randChoice == 0)
				return new InetSocketAddress(rand.nextInt(0xFFFF));
			else if (randChoice == 1)
				try {
					return new InetSocketAddress(InetAddress.getByAddress(new byte[] { (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF) }),
							rand.nextInt(0xFFFF));
				} catch (UnknownHostException e) {
					e.printStackTrace();
					return null;
				}
			else if (randChoice == 2)
				try {
					return new InetSocketAddress(InetAddress.getByAddress(new byte[] { (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF),
							(byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF),
							(byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF), (byte) rand.nextInt(0xFF) }),
							rand.nextInt(0xFFFF));
				} catch (UnknownHostException e) {
					e.printStackTrace();
					return null;
				}
			else
				return InetSocketAddress.createUnresolved((String) generateRandomValue(String.class, rand.nextLong(), maxSize, testedTypes), rand.nextInt(0xFFFF));
		} else if (type.equals(Selection.class)) {
			Class<?> selectionType;
			do
				selectionType = testedTypes[rand.nextInt(testedTypes.length)];
			while (selectionType.isPrimitive());
			Object[] array = (Object[]) Array.newInstance(selectionType, rand.nextInt(maxSize));
			for (int i = 0; i < array.length; i++)
				array[i] = generateRandomValue(selectionType, rand.nextLong(), maxSize, testedTypes);
			return new Selection<>(new HashSet<>(Arrays.asList(array)), (Class<Object>) selectionType);
		} else if (type.equals(Point2i.class))
			return new Point2i(rand.nextInt(), rand.nextInt());
		else if (type.equals(Point2f.class))
			return new Point2f(rand.nextFloat(), rand.nextFloat());
		else if (type.equals(Point2d.class))
			return new Point2d(rand.nextDouble(), rand.nextDouble());
		else if (type.equals(Point3i.class))
			return new Point3i(rand.nextInt(), rand.nextInt(), rand.nextInt());
		else if (type.equals(Point3f.class))
			return new Point3f(rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
		else if (type.equals(Point3d.class))
			return new Point3d(rand.nextDouble(), rand.nextDouble(), rand.nextDouble());
		else if (type.equals(Point4i.class))
			return new Point4i(rand.nextInt(), rand.nextInt(), rand.nextInt(), rand.nextInt());
		else if (type.equals(Point4f.class))
			return new Point4f(rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
		else if (type.equals(Point4d.class))
			return new Point4d(rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble());
		else if (type.equals(Vector2f.class))
			return new Vector2f(rand.nextFloat(), rand.nextFloat());
		else if (type.equals(Vector2d.class))
			return new Vector2d(rand.nextDouble(), rand.nextDouble());
		else if (type.equals(Vector3f.class))
			return new Vector3f(rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
		else if (type.equals(Vector3d.class))
			return new Vector3d(rand.nextDouble(), rand.nextDouble(), rand.nextDouble());
		else if (type.equals(Vector4f.class))
			return new Vector4f(rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
		else if (type.equals(Vector4d.class))
			return new Vector4d(rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble());
		else if (type.equals(GVector.class)) {
			double[] val = new double[rand.nextInt(maxSize)];
			for (int i = 0; i < val.length; i++)
				val[i] = rand.nextDouble();
			return new GVector(val);
		} else if (type.equals(Matrix3f.class))
			return new Matrix3f(rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
		else if (type.equals(Matrix3d.class))
			return new Matrix3d(rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(),
					rand.nextDouble());
		else if (type.equals(Matrix4f.class))
			return new Matrix4f(rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(),
					rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
		else if (type.equals(Matrix4d.class))
			return new Matrix4d(rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(),
					rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), rand.nextDouble());
		else if (type.equals(GMatrix.class)) {
			int size = rand.nextInt(maxSize);
			double[] val = new double[size * size];
			for (int i = 0; i < val.length; i++)
				val[i] = rand.nextDouble();
			return new GMatrix(size, size, val);
		} else if (type.equals(LocalDate.class))
			return LocalDate.of(Year.MIN_VALUE + rand.nextInt(Year.MAX_VALUE - Year.MIN_VALUE), 1 + rand.nextInt(11), 1 + rand.nextInt(28));
		else if (type.equals(LocalTime.class))
			return LocalTime.of(rand.nextInt(23), rand.nextInt(60), rand.nextInt(60), rand.nextInt(999_999_999));
		else if (type.equals(LocalDateTime.class))
			return LocalDateTime.of((LocalDate) generateRandomValue(LocalDate.class, rand.nextLong(), maxSize, testedTypes),
					(LocalTime) generateRandomValue(LocalTime.class, rand.nextLong(), maxSize, testedTypes));
		else if (type.equals(BitSet.class)) {
			BitSet bs = new BitSet(rand.nextInt(maxSize));
			for (int i = 0; i < bs.size(); i++)
				if (rand.nextBoolean())
					bs.set(i);
			return bs;
		} else if (type.equals(OpenClass.class)) {
			OpenClass value = new OpenClass();
			value.testBoolean = rand.nextBoolean();
			value.testByte = (byte) rand.nextInt();
			value.testChar = (char) rand.nextInt();
			value.testShort = (short) rand.nextInt();
			value.testInt = rand.nextInt();
			value.testLong = rand.nextLong();
			value.testFloat = rand.nextFloat();
			value.testDouble = rand.nextDouble();
			value.testString = randomString(rand, rand.nextInt(156));
			return value;
		} else if (type.equals(SerialisationMultipleVersion.class)) {
			SerialisationMultipleVersion value = new SerialisationMultipleVersion();
			value.testBoolean = rand.nextBoolean();
			value.testByte = (byte) rand.nextInt();
			value.testChar = (char) rand.nextInt();
			value.testShort = (short) rand.nextInt();
			value.testInt = rand.nextInt();
			value.testLong = rand.nextLong();
			value.testFloat = rand.nextFloat();
			value.testDouble = rand.nextDouble();
			value.testString = randomString(rand, rand.nextInt(156));
			return value;
		} else if (type.equals(OpenClassSerialisable.class)) {
			OpenClassSerialisable value = new OpenClassSerialisable();
			value.testBoolean = rand.nextBoolean();
			value.testByte = (byte) rand.nextInt();
			value.testChar = (char) rand.nextInt();
			value.testShort = (short) rand.nextInt();
			value.testInt = rand.nextInt();
			value.testLong = rand.nextLong();
			value.testFloat = rand.nextFloat();
			value.testDouble = rand.nextDouble();
			value.testString = randomString(rand, rand.nextInt(156));
			return value;
		} else if (type.equals(OpenClassWithEditor.class)) {
			OpenClassWithEditor value = new OpenClassWithEditor();
			value.testBoolean = rand.nextBoolean();
			value.testByte = (byte) rand.nextInt();
			value.testChar = (char) rand.nextInt();
			value.testShort = (short) rand.nextInt();
			value.testInt = rand.nextInt();
			value.testLong = rand.nextLong();
			value.testFloat = rand.nextFloat();
			value.testDouble = rand.nextDouble();
			value.testString = randomString(rand, rand.nextInt(156));
			return value;
		} else if (type.equals(OpenClassWithFileRecorder.class)) {
			OpenClassWithFileRecorder value = new OpenClassWithFileRecorder();
			value.testBoolean = rand.nextBoolean();
			value.testByte = (byte) rand.nextInt();
			value.testChar = (char) rand.nextInt();
			value.testShort = (short) rand.nextInt();
			value.testInt = rand.nextInt();
			value.testLong = rand.nextLong();
			value.testFloat = rand.nextFloat();
			value.testDouble = rand.nextDouble();
			return value;
		} else if (type.equals(OpenClassWithStreamer.class)) {
			OpenClassWithStreamer value = new OpenClassWithStreamer();
			value.testBoolean = rand.nextBoolean();
			value.testByte = (byte) rand.nextInt();
			value.testChar = (char) rand.nextInt();
			value.testShort = (short) rand.nextInt();
			value.testInt = rand.nextInt();
			value.testLong = rand.nextLong();
			value.testFloat = rand.nextFloat();
			value.testDouble = rand.nextDouble();
			value.testString = randomString(rand, rand.nextInt(156));
			return value;
		} else if (type.equals(PrivateConstructor.class)) {
			// BaseTestClass value = new BaseTestClass();
			PrivateConstructor value = PrivateConstructor.create();
			value.testBoolean = rand.nextBoolean();
			value.testByte = (byte) rand.nextInt();
			value.testChar = (char) rand.nextInt();
			value.testShort = (short) rand.nextInt();
			value.testInt = rand.nextInt();
			value.testLong = rand.nextLong();
			value.testFloat = rand.nextFloat();
			value.testDouble = rand.nextDouble();
			value.testString = randomString(rand, rand.nextInt(156));
			return value;
		} else if (type.equals(PrivateConstructorSerialisable.class)) {
			// BaseTestClass value = new BaseTestClass();
			PrivateConstructorSerialisable value = PrivateConstructorSerialisable.create();
			value.testBoolean = rand.nextBoolean();
			value.testByte = (byte) rand.nextInt();
			value.testChar = (char) rand.nextInt();
			value.testShort = (short) rand.nextInt();
			value.testInt = rand.nextInt();
			value.testLong = rand.nextLong();
			value.testFloat = rand.nextFloat();
			value.testDouble = rand.nextDouble();
			value.testString = randomString(rand, rand.nextInt(156));
			return value;
		} else if (type.equals(PrivateConstructorWithEditor.class)) {
			// BaseTestClass value = new BaseTestClass();
			PrivateConstructorWithEditor value = PrivateConstructorWithEditor.create();
			value.testBoolean = rand.nextBoolean();
			value.testByte = (byte) rand.nextInt();
			value.testChar = (char) rand.nextInt();
			value.testShort = (short) rand.nextInt();
			value.testInt = rand.nextInt();
			value.testLong = rand.nextLong();
			value.testFloat = rand.nextFloat();
			value.testDouble = rand.nextDouble();
			value.testString = randomString(rand, rand.nextInt(156));
			return value;
		} else if (type.equals(PrivateConstructorWithFileRecorder.class)) {
			// BaseTestClass value = new BaseTestClass();
			PrivateConstructorWithFileRecorder value = PrivateConstructorWithFileRecorder.create();
			value.testBoolean = rand.nextBoolean();
			value.testByte = (byte) rand.nextInt();
			value.testChar = (char) rand.nextInt();
			value.testShort = (short) rand.nextInt();
			value.testInt = rand.nextInt();
			value.testLong = rand.nextLong();
			value.testFloat = rand.nextFloat();
			value.testDouble = rand.nextDouble();
			return value;
		} else if (type.equals(PrivateConstructorWithStreamer.class)) {
			// BaseTestClass value = new BaseTestClass();
			PrivateConstructorWithStreamer value = PrivateConstructorWithStreamer.create();
			value.testBoolean = rand.nextBoolean();
			value.testByte = (byte) rand.nextInt();
			value.testChar = (char) rand.nextInt();
			value.testShort = (short) rand.nextInt();
			value.testInt = rand.nextInt();
			value.testLong = rand.nextLong();
			value.testFloat = rand.nextFloat();
			value.testDouble = rand.nextDouble();
			value.testString = randomString(rand, rand.nextInt(156));
			return value;
		} else if (type.equals(FixeSizeClass.class))
			return new FixeSizeClass(rand.nextBoolean(), rand.nextInt(), rand.nextLong(), rand.nextFloat());
		else if (type.equals(FullConstructor.class))
			return new FullConstructor(rand.nextBoolean(), (byte) rand.nextInt(), (char) rand.nextInt(), (short) rand.nextInt(), rand.nextInt(), rand.nextLong(), rand.nextFloat(), rand.nextDouble(),
					randomString(rand, rand.nextInt(156)));
		else if (type.equals(FullConstructorSerialisable.class))
			return new FullConstructorSerialisable(rand.nextBoolean(), (byte) rand.nextInt(), (char) rand.nextInt(), (short) rand.nextInt(), rand.nextInt(), rand.nextLong(), rand.nextFloat(),
					rand.nextDouble(), randomString(rand, rand.nextInt(156)));
		else if (type.equals(FullConstructorWithEditor.class))
			return new FullConstructorWithEditor(rand.nextBoolean(), (byte) rand.nextInt(), (char) rand.nextInt(), (short) rand.nextInt(), rand.nextInt(), rand.nextLong(), rand.nextFloat(),
					rand.nextDouble(), randomString(rand, rand.nextInt(156)));
		else if (type.equals(FullConstructorWithFileRecorder.class))
			return new FullConstructorWithFileRecorder(rand.nextBoolean(), (byte) rand.nextInt(), (char) rand.nextInt(), (short) rand.nextInt(), rand.nextInt(), rand.nextLong(), rand.nextFloat(),
					rand.nextDouble());
		else if (type.equals(FullConstructorWithStreamer.class))
			return new FullConstructorWithStreamer(rand.nextBoolean(), (byte) rand.nextInt(), (char) rand.nextInt(), (short) rand.nextInt(), rand.nextInt(), rand.nextLong(), rand.nextFloat(),
					rand.nextDouble(), randomString(rand, rand.nextInt(156)));
		else if (type.equals(BaseTestClass2.class))
			return new BaseTestClass2(rand.nextBoolean(), (byte) rand.nextInt(), (char) rand.nextInt(), (short) rand.nextInt(), rand.nextInt(), rand.nextLong(), rand.nextFloat(), rand.nextDouble(),
					randomString(rand, rand.nextInt(156)));
		else
			throw new IllegalArgumentException("No generator for type: " + type);
	}
}
