/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package test.scenarium.river;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

import io.beanmanager.consumer.EditorConsumer;
import io.beanmanager.editors.PropertyEditorManager;
import io.scenarium.river.consumer.DataStreamConsumer;
import io.scenarium.river.consumer.FileStreamConsumer;
import io.scenarium.river.datastream.DataStreamManager;
import io.scenarium.river.filerecorder.FileStreamManager;
import io.scenarium.river.filerecorder.reader.FileStreamReader;
import io.scenarium.river.filerecorder.reader.PropertyStreamReader;
import io.scenarium.river.filerecorder.reader.SerializableObjectStreamReader;
import io.scenarium.river.filerecorder.reader.StreamableObjectStreamReader;
import io.scenarium.river.filerecorder.recorder.FileStreamRecorder;
import io.scenarium.river.filerecorder.recorder.PropertyStreamRecorder;
import io.scenarium.river.filerecorder.recorder.SerializableObjectStreamRecorder;
import io.scenarium.river.filerecorder.recorder.StreamableObjectStreamRecorder;
import io.scenarium.river.internal.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
//import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import test.scenarium.river.datastream.input.FixeSizeClassInputStream;
import test.scenarium.river.datastream.output.FixeSizeClassOutputStream;
import test.scenarium.river.editor.FixeSizeClassEditor;
import test.scenarium.river.filerecorder.reader.FixeSizeClassReader;
import test.scenarium.river.filerecorder.recorder.FixeSizeClassRecorder;
import test.scenarium.river.modelTest.FixeSizeClass;

@TestMethodOrder(OrderAnnotation.class)
public class TestperformenceFile {
	private final int nbCycleTest = 100000;

	private static String randomString(int count) {
		Random rand = new Random(System.nanoTime());
		String s = new String();
		int nbChar = count;
		for (int i = 0; i < nbChar; i++) {
			char c = (char) rand.nextInt();
			while ((c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c < '0' || c > '9'))
				c = (char) rand.nextInt();
			s = s + c;
		}
		return s;
	}

	private static final Path TMP_FOLDER = getTemporaryFolder();

	private static Path getTemporaryFolder() {
		String strTmp = System.getProperty("java.io.tmpdir");
		Path out = Paths.get(strTmp + FileSystems.getDefault().getSeparator() + "scRiverTest" + FileSystems.getDefault().getSeparator() + randomString(25));
		try {
			createFolder(out);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return out;
	}

	private static void createFolder(Path path) throws IOException {
		if (!Files.exists(path)) {
			Log.print("Create folder: " + path);
			Files.createDirectories(path);
		}
	}

	private Long totalTimeWrite = (long) 0;
	private Long totalTimeRead = (long) 0;

	public double performTestPerformence(String basePrint, Class<?> testType, int nbCycle, int nbBlobSize) {

		for (int kkk = 0; kkk < nbCycle; kkk++) {

			// check if it exist
			assertTrue(FileStreamManager.hasStreamRecorder(testType));

			final FileStreamRecorder output = FileStreamManager.getRecorder(testType);
			assertNotNull(output);
			final FileStreamReader input = FileStreamManager.getReader(testType);
			assertNotNull(input);

			this.totalTimeWrite = (long) 0;
			this.totalTimeRead = (long) 0;

			output.setPath(TMP_FOLDER.toString());
			assertEquals(output.getPath(), TMP_FOLDER.toString());
			output.setName("testFile");
			assertEquals(output.getName(), "testFile");

			// Create a list of object and serialize each.
			for (int iii = 0; iii < nbBlobSize; iii++) {
				Object tmpData = ValueGenerator.generateRandomValue(testType, iii, 50, new Class<?>[0]);
				assertDoesNotThrow(() -> {
					long startTime = System.nanoTime();
					output.push(tmpData);
					long delta = System.nanoTime() - startTime;
					this.totalTimeWrite += delta;
				});
			}
			// Close the output:
			assertDoesNotThrow(() -> {
				output.close();
			});

			input.setPath(TMP_FOLDER.toString());
			assertEquals(TMP_FOLDER.toString(), input.getPath());
			input.setName("testFile");
			assertEquals("testFile", input.getName());

			for (int iii = 0; iii < nbBlobSize; iii++)
				assertDoesNotThrow(() -> {
					long startTime = System.nanoTime();
					Object tmpData = input.pop();
					long delta = System.nanoTime() - startTime;
					this.totalTimeRead += delta;
				});
			double readSpeed = (double) this.totalTimeRead / (double) nbBlobSize;
			double writeSpeed = (double) this.totalTimeWrite / (double) nbBlobSize;
			Log.print(basePrint + " generate results in : w=" + writeSpeed + " r=" + readSpeed);
		}
		return 0;
	}

	@Before
	public void initSerializer() {
		// clean all test serialize
		DataStreamManager.purgeStream(TestperformenceFile.class.getModule());
		FileStreamManager.purgeStream(TestperformenceFile.class.getModule());
		PropertyEditorManager.purgeEditors(TestperformenceFile.class.getModule());
	}

	@Test
	public void serializableSpeed() {
		Class<?> testType = FixeSizeClass.class;

		FileStreamRecorder output;
		FileStreamReader input;
		output = FileStreamManager.getRecorder(testType);
		assertTrue(output instanceof SerializableObjectStreamRecorder);
		input = FileStreamManager.getReader(testType);
		assertTrue(input instanceof SerializableObjectStreamReader);

		// performence test here
		performTestPerformence("File serialisable ", testType, 1, this.nbCycleTest);
	}

	@Test
	public void editableSpeed() {

		Class<?> testType = FixeSizeClass.class;

		EditorConsumer consumerEditor = new EditorConsumer();
		consumerEditor.accept(FixeSizeClass.class, FixeSizeClassEditor.class);
		FileStreamRecorder output;
		FileStreamReader input;
		output = FileStreamManager.getRecorder(testType);
		assertTrue(output instanceof PropertyStreamRecorder);
		input = FileStreamManager.getReader(testType);
		assertTrue(input instanceof PropertyStreamReader);

		// performence test here
		performTestPerformence("File editable     ", testType, 1, this.nbCycleTest);

		// Remove streamer with module reference
		PropertyEditorManager.purgeEditors(FixeSizeClass.class.getModule());
		output = FileStreamManager.getRecorder(testType);
		assertTrue(output instanceof SerializableObjectStreamRecorder);
		input = FileStreamManager.getReader(testType);
		assertTrue(input instanceof SerializableObjectStreamReader);
	}

	@Test
	public void streamableSpeed() {

		Class<?> testType = FixeSizeClass.class;
		FileStreamRecorder output;
		FileStreamReader input;
		DataStreamConsumer consumer = new DataStreamConsumer();
		consumer.accept(FixeSizeClass.class, FixeSizeClassInputStream.class, FixeSizeClassOutputStream.class);
		output = FileStreamManager.getRecorder(testType);
		assertTrue(output instanceof StreamableObjectStreamRecorder);
		input = FileStreamManager.getReader(testType);
		assertTrue(input instanceof StreamableObjectStreamReader);

		// performence test here
		performTestPerformence("File streamable   ", testType, 1, this.nbCycleTest);

		// Remove streamer with module reference
		DataStreamManager.purgeStream(FixeSizeClass.class.getModule());
		output = FileStreamManager.getRecorder(testType);
		assertTrue(output instanceof SerializableObjectStreamRecorder);
		input = FileStreamManager.getReader(testType);
		assertTrue(input instanceof SerializableObjectStreamReader);
	}

	@Test
	public void recordableSpeed() {

		Class<?> testType = FixeSizeClass.class;
		FileStreamRecorder output;
		FileStreamReader input;
		FileStreamConsumer consumerFile = new FileStreamConsumer();
		consumerFile.accept(FixeSizeClass.class, FixeSizeClassReader.EXTENTION, FixeSizeClassReader.class, FixeSizeClassRecorder.class);
		output = FileStreamManager.getRecorder(testType);
		assertFalse(output instanceof PropertyStreamRecorder);
		assertFalse(output instanceof StreamableObjectStreamRecorder);
		assertFalse(output instanceof SerializableObjectStreamRecorder);
		input = FileStreamManager.getReader(testType);
		assertFalse(input instanceof PropertyStreamReader);
		assertFalse(input instanceof StreamableObjectStreamReader);
		assertFalse(input instanceof SerializableObjectStreamReader);
		// performence test here
		performTestPerformence("File recordable   ", testType, 1, this.nbCycleTest);

		// Remove streamer with module reference
		FileStreamManager.purgeStream(FixeSizeClass.class.getModule());
		output = FileStreamManager.getRecorder(testType);
		assertTrue(output instanceof SerializableObjectStreamRecorder);
		input = FileStreamManager.getReader(testType);
		assertTrue(input instanceof SerializableObjectStreamReader);
	}

}
