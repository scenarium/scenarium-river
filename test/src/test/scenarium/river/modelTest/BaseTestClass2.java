package test.scenarium.river.modelTest;

import java.io.Serializable;

public class BaseTestClass2 implements Serializable {
	private static final long serialVersionUID = 1L;
	public final boolean testBoolean;
	public final byte testByte;
	public final char testChar;
	public final short testShort;
	public final int testInt;
	public final long testLong;
	public final float testFloat;
	public final double testDouble;
	public final String testString;

	public BaseTestClass2(boolean testBoolean, byte testByte, char testChar, short testShort, int testInt, long testLong, float testFloat, double testDouble, String testString) {
		super();
		this.testBoolean = testBoolean;
		this.testByte = testByte;
		this.testChar = testChar;
		this.testShort = testShort;
		this.testInt = testInt;
		this.testLong = testLong;
		this.testFloat = testFloat;
		this.testDouble = testDouble;
		this.testString = testString;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseTestClass2 other = (BaseTestClass2) obj;
		if (this.testBoolean != other.testBoolean)
			return false;
		if (this.testByte != other.testByte)
			return false;
		if (this.testChar != other.testChar)
			return false;
		if (this.testShort != other.testShort)
			return false;
		if (this.testInt != other.testInt)
			return false;
		if (this.testLong != other.testLong)
			return false;
		if (this.testFloat != other.testFloat)
			return false;
		if (this.testDouble != other.testDouble)
			return false;
		if (!this.testString.contentEquals(other.testString))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
