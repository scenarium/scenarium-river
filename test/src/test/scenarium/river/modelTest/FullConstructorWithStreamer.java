package test.scenarium.river.modelTest;

public class FullConstructorWithStreamer {
	public static final long serialVersionUID = 1L;
	private final boolean testBoolean;
	private final byte testByte;
	private final char testChar;
	private final short testShort;
	private final int testInt;
	private final long testLong;
	private final float testFloat;
	private final double testDouble;
	private final String testString;

	public FullConstructorWithStreamer(boolean testBoolean, byte testByte, char testChar, short testShort, int testInt, long testLong, float testFloat, double testDouble, String testString) {
		super();
		this.testBoolean = testBoolean;
		this.testByte = testByte;
		this.testChar = testChar;
		this.testShort = testShort;
		this.testInt = testInt;
		this.testLong = testLong;
		this.testFloat = testFloat;
		this.testDouble = testDouble;
		this.testString = testString;
	}

	public boolean isTestBoolean() {
		return this.testBoolean;
	}

	public byte getTestByte() {
		return this.testByte;
	}

	public char getTestChar() {
		return this.testChar;
	}

	public short getTestShort() {
		return this.testShort;
	}

	public int getTestInt() {
		return this.testInt;
	}

	public long getTestLong() {
		return this.testLong;
	}

	public float getTestFloat() {
		return this.testFloat;
	}

	public double getTestDouble() {
		return this.testDouble;
	}

	public String getTestString() {
		return this.testString;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FullConstructorWithStreamer other = (FullConstructorWithStreamer) obj;
		if (this.testBoolean != other.testBoolean)
			return false;
		if (this.testByte != other.testByte)
			return false;
		if (this.testChar != other.testChar)
			return false;
		if (this.testShort != other.testShort)
			return false;
		if (this.testInt != other.testInt)
			return false;
		if (this.testLong != other.testLong)
			return false;
		if (this.testFloat != other.testFloat)
			return false;
		if (this.testDouble != other.testDouble)
			return false;
		if (!this.testString.contentEquals(other.testString))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
