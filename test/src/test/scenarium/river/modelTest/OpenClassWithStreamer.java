package test.scenarium.river.modelTest;

public class OpenClassWithStreamer {
	public static final long serialVersionUID = 1L;
	public boolean testBoolean;
	public byte testByte;
	public char testChar;
	public short testShort;
	public int testInt;
	public long testLong;
	public float testFloat;
	public double testDouble;
	public String testString;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OpenClassWithStreamer other = (OpenClassWithStreamer) obj;
		if (this.testBoolean != other.testBoolean)
			return false;
		if (this.testByte != other.testByte)
			return false;
		if (this.testChar != other.testChar)
			return false;
		if (this.testShort != other.testShort)
			return false;
		if (this.testInt != other.testInt)
			return false;
		if (this.testLong != other.testLong)
			return false;
		if (this.testFloat != other.testFloat)
			return false;
		if (this.testDouble != other.testDouble)
			return false;
		if (!this.testString.contentEquals(other.testString))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
