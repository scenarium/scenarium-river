package test.scenarium.river.modelTest;

import java.io.Serializable;

public class FixeSizeClass implements Serializable {
	public static final long serialVersionUID = 1L;
	private final boolean testBoolean;
	private final int testInt;
	private final long testLong;
	private final float testFloat;

	public FixeSizeClass(boolean testBoolean, int testInt, long testLong, float testFloat) {
		super();
		this.testBoolean = testBoolean;
		this.testInt = testInt;
		this.testLong = testLong;
		this.testFloat = testFloat;
	}

	public boolean isTestBoolean() {
		return this.testBoolean;
	}

	public int getTestInt() {
		return this.testInt;
	}

	public long getTestLong() {
		return this.testLong;
	}

	public float getTestFloat() {
		return this.testFloat;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FixeSizeClass other = (FixeSizeClass) obj;
		if (this.testBoolean != other.testBoolean)
			return false;
		if (this.testInt != other.testInt)
			return false;
		if (this.testLong != other.testLong)
			return false;
		if (this.testFloat != other.testFloat)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
