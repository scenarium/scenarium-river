/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package test.scenarium.river;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;

import io.beanmanager.consumer.EditorConsumer;
import io.beanmanager.editors.PropertyEditorManager;
import io.scenarium.river.consumer.DataStreamConsumer;
import io.scenarium.river.datastream.DataStreamManager;
import io.scenarium.river.datastream.input.DataFlowInputStream;
import io.scenarium.river.datastream.input.PropertyInputStream;
import io.scenarium.river.datastream.input.SerializableInputStream;
import io.scenarium.river.datastream.output.DataFlowOutputStream;
import io.scenarium.river.datastream.output.PropertyOutputStream;
import io.scenarium.river.datastream.output.SerializableOutputStream;
import io.scenarium.river.filerecorder.FileStreamManager;
import io.scenarium.river.internal.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
//import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import test.scenarium.river.datastream.input.FixeSizeClassInputStream;
import test.scenarium.river.datastream.output.FixeSizeClassOutputStream;
import test.scenarium.river.editor.FixeSizeClassEditor;
import test.scenarium.river.modelTest.FixeSizeClass;

@TestMethodOrder(OrderAnnotation.class)
public class TestperformenceBinary {

	private Long totalTimeWrite = (long) 0;
	private Long totalTimeRead = (long) 0;

	public double performTestPerformence(String basePrint, Class<?> testType, int nbCycle, int nbBlobSize) {

		for (int kkk = 0; kkk < nbCycle; kkk++) {
			// Create output stream to generate serialization of the data
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DataOutputStream dos = new DataOutputStream(baos);
			final DataFlowOutputStream<?> output = DataStreamManager.createOutputStream(testType);
			assertNotNull(output);
			output.setDataOutput(dos);
			this.totalTimeWrite = (long) 0;
			for (int iii = 0; iii < nbBlobSize; iii++) {
				Object tmpData = ValueGenerator.generateRandomValue(testType, iii, 50, new Class<?>[0]);
				assertDoesNotThrow(() -> {
					long startTime = System.nanoTime();
					output.pushObject(tmpData);
					long delta = System.nanoTime() - startTime;
					this.totalTimeWrite += delta;
				});
			}

			// check if exist:
			assertTrue(DataStreamManager.hasInputStream(testType));

			// Create input stream to retrieve deserialized data
			DataInputStream dis = new DataInputStream(new ByteArrayInputStream(baos.toByteArray()));
			final DataFlowInputStream<?> input = DataStreamManager.createInputStream(testType);
			assertNotNull(input);
			input.setDataInput(dis);

			this.totalTimeRead = (long) 0;
			for (int iii = 0; iii < nbBlobSize; iii++)
				assertDoesNotThrow(() -> {
					long startTime = System.nanoTime();
					Object tmpData;
					tmpData = input.pop();
					long delta = System.nanoTime() - startTime;
					this.totalTimeRead += delta;
				});
			double readSpeed = (double) this.totalTimeRead / (double) nbBlobSize;
			double writeSpeed = (double) this.totalTimeWrite / (double) nbBlobSize;
			Log.print(basePrint + " generate results in : w=" + writeSpeed + " r=" + readSpeed);
		}
		return 0;
	}

	@Before
	public void initSerializer() {
		// clean all test serialize
		DataStreamManager.purgeStream(TestperformenceBinary.class.getModule());
		FileStreamManager.purgeStream(TestperformenceBinary.class.getModule());
		PropertyEditorManager.purgeEditors(TestperformenceBinary.class.getModule());
	}

	@Test
	public void serializableSpeed() {
		Class<?> testType = FixeSizeClass.class;
		DataFlowOutputStream<?> output = null;
		DataFlowInputStream<?> input = null;
		output = DataStreamManager.createOutputStream(testType);
		assertTrue(output instanceof SerializableOutputStream);
		input = DataStreamManager.createInputStream(testType);
		assertTrue(input instanceof SerializableInputStream);

		// performence test here
		performTestPerformence("serialisable", testType, 1, 1000000);
	}

	@Test
	public void editableSpeed() {

		Class<?> testType = FixeSizeClass.class;
		DataFlowOutputStream<?> output = null;
		DataFlowInputStream<?> input = null;

		EditorConsumer consumerEditor = new EditorConsumer();
		consumerEditor.accept(FixeSizeClass.class, FixeSizeClassEditor.class);
		output = DataStreamManager.createOutputStream(testType);
		// is streamer ?
		assertTrue(output instanceof PropertyOutputStream);
		input = DataStreamManager.createInputStream(testType);
		// is streamer ?
		assertTrue(input instanceof PropertyInputStream);

		// performence test here
		performTestPerformence("editable     ", testType, 1, 1000000);

		// Remove streamer with module reference
		PropertyEditorManager.purgeEditors(FixeSizeClass.class.getModule());
		output = DataStreamManager.createOutputStream(testType);
		assertTrue(output instanceof SerializableOutputStream);
		input = DataStreamManager.createInputStream(testType);
		assertTrue(input instanceof SerializableInputStream);
	}

	@Test
	public void streamableSpeed() {

		Class<?> testType = FixeSizeClass.class;
		DataFlowOutputStream<?> output = null;
		DataFlowInputStream<?> input = null;
		DataStreamConsumer consumer = new DataStreamConsumer();
		consumer.accept(FixeSizeClass.class, FixeSizeClassInputStream.class, FixeSizeClassOutputStream.class);
		output = DataStreamManager.createOutputStream(testType);
		// is streamer ?
		assertFalse(output instanceof PropertyOutputStream);
		assertFalse(output instanceof SerializableOutputStream);
		input = DataStreamManager.createInputStream(testType);
		// is streamer ?
		assertFalse(input instanceof PropertyInputStream);
		assertFalse(input instanceof SerializableInputStream);

		// performence test here
		performTestPerformence("streamable   ", testType, 1, 1000000);

		// Remove streamer with module reference
		DataStreamManager.purgeStream(FixeSizeClass.class.getModule());
		output = DataStreamManager.createOutputStream(testType);
		assertTrue(output instanceof SerializableOutputStream);
		input = DataStreamManager.createInputStream(testType);
		assertTrue(input instanceof SerializableInputStream);
	}

}
