/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.consumer;

import io.beanmanager.consumer.EditorConsumer;
import io.scenarium.river.datastream.DataStreamManager;
import io.scenarium.river.datastream.input.DataFlowInputStream;
import io.scenarium.river.datastream.output.DataFlowOutputStream;

public class DataStreamConsumer extends EditorConsumer {
	public <T> void accept(Class<T> type, Class<? extends DataFlowInputStream<T>> inputStream, Class<? extends DataFlowOutputStream<T>> outputStream) {
		DataStreamManager.registerStream(type, inputStream, outputStream);
	}
}
