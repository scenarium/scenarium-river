/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.consumer;

import io.beanmanager.consumer.EditorConsumer;
import io.scenarium.river.filerecorder.FileStreamManager;
import io.scenarium.river.filerecorder.reader.FileStreamReader;
import io.scenarium.river.filerecorder.recorder.FileStreamRecorder;

public class FileStreamConsumer extends EditorConsumer {
	public <T> void accept(Class<T> type, String extention, Class<? extends FileStreamReader> inputStream, Class<? extends FileStreamRecorder> outputStream) {
		FileStreamManager.registerStream(type, extention, inputStream, outputStream);
	}
}
