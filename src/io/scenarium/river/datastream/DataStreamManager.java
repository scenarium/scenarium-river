/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.datastream;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import io.beanmanager.editors.PropertyEditor;
import io.beanmanager.editors.PropertyEditorManager;
import io.beanmanager.editors.container.BeanEditor;
import io.scenarium.river.datastream.input.DataFlowInputStream;
import io.scenarium.river.datastream.input.PropertyInputStream;
import io.scenarium.river.datastream.input.SerializableInputStream;
import io.scenarium.river.datastream.output.DataFlowOutputStream;
import io.scenarium.river.datastream.output.PropertyOutputStream;
import io.scenarium.river.datastream.output.SerializableOutputStream;

/** This Singleton permit to get a streamer associated with a specific Type of class */
public final class DataStreamManager {
	/** Static class ==> can not be construct. */
	private DataStreamManager() {}

	/** List of INPUT Streamer recorded by the user */
	private static ConcurrentHashMap<Class<?>, Class<? extends DataFlowInputStream<?>>> inputStreams = new ConcurrentHashMap<>();
	/** List of OUTPUT Streamer recorded by the user */
	private static ConcurrentHashMap<Class<?>, Class<? extends DataFlowOutputStream<?>>> outputStreams = new ConcurrentHashMap<>();
	/** Cache of available output streamer in this interface */
	private static HashMap<Class<?>, Boolean> hasOutputStream = new HashMap<>();
	/** Cache of available input streamer in this interface */
	private static HashMap<Class<?>, Boolean> hasInputStream = new HashMap<>();

	/**
	 * Create an input streamer associated with the request type (search only in the local streamer).
	 * @param type Type that want to be serialized
	 * @return An implementation of an input streamer of this specific class.
	 */
	public static DataFlowInputStream<?> createInputStreamOnly(Class<?> type) {
		// [1] Check if a streamer has been registered:
		Class<? extends DataFlowInputStream<?>> inputStreamClass = inputStreams.get(type);
		if (inputStreamClass != null)
			try {
				return inputStreamClass.getConstructor().newInstance();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				// this is a critical situation...
				e.printStackTrace();
			}
		return null;
	}

	/**
	 * Create an input streamer associated with the request type.
	 * Search in (in sequence):
	 *   - The local streamer available
	 *   - A property editor from bean manager (not a BeanEditor)
	 *   - A serializable object streamer
	 *   - null
	 * @param type Type that want to be serialized
	 * @return An implementation of an input streamer of this specific class.
	 */
	public static DataFlowInputStream<?> createInputStream(Class<?> type) {
		// [1] Check if a streamer has been registered:
		Class<? extends DataFlowInputStream<?>> inputStreamClass = inputStreams.get(type);
		if (inputStreamClass != null)
			try {
				return inputStreamClass.getConstructor().newInstance();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				// this is a critical situation...
				e.printStackTrace();
				return null;
			}
		// [2] Try to find an editor
		PropertyEditor<?> pe = PropertyEditorManager.findEditor(type, "");
		// remove bean editor ==> not a serializable model
		if (pe instanceof BeanEditor<?>)
			pe = null;
		// find an editor ==> create associated stream
		if (pe != null)
			return new PropertyInputStream<>(pe);
		// [3] Check if stream is serializable
		if (Serializable.class.isAssignableFrom(type))
			return new SerializableInputStream<>();
		// [4] can NOT find a streamer
		return null;
	}

	/**
	 * Create an output streamer associated with the request type (search only in the local streamer).
	 * @param type Type that want to be serialized
	 * @return An implementation of an output streamer of this specific class.
	 */
	public static DataFlowOutputStream<?> createOutputStreamOnly(Class<?> type) {
		// [1] Check if a streamer has been registered:
		Class<? extends DataFlowOutputStream<?>> outputStreamClass = outputStreams.get(type);
		if (outputStreamClass != null)
			try {
				return outputStreamClass.getConstructor().newInstance();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				// this is a critical situation...
				e.printStackTrace();
			}
		return null;
	}

	/**
	 * Create an output streamer associated with the request type.
	 * Search in (in sequence):
	 *   - The local streamer available
	 *   - A property editor from bean manager (not a BeanEditor)
	 *   - A serializable object streamer
	 *   - null
	 * @param type Type that want to be serialized
	 * @return An implementation of an output streamer of this specific class.
	 */
	public static DataFlowOutputStream<?> createOutputStream(Class<?> type) {
		// [1] Check if a streamer has been registered:
		Class<? extends DataFlowOutputStream<?>> outputStreamClass = outputStreams.get(type);
		if (outputStreamClass != null)
			try {
				return outputStreamClass.getConstructor().newInstance();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				// this is a critical situation...
				e.printStackTrace();
				return null;
			}
		// [2] Try to find an editor
		PropertyEditor<?> pe = PropertyEditorManager.findEditor(type, "");
		// remove bean editor ==> not a serializable model
		if (pe instanceof BeanEditor<?>)
			pe = null;
		// find an editor ==> create associated stream
		if (pe != null)
			return new PropertyOutputStream<>(pe);
		// [3] Check if stream is serializable
		if (Serializable.class.isAssignableFrom(type))
			return new SerializableOutputStream<>();
		// [4] can NOT find a streamer
		return null;
	}

	/**
	 * Verify if the type is streamable (in the register list only.
	 * @param type Type that want to be serialized
	 * @return true: the type is streamable.
	 *         false: This is not possible
	 */
	public static boolean hasOutputStreamOnly(Class<? extends Object> type) {
		return outputStreams.get(type) != null;
	}

	/**
	 * Verify if the type is streamable.
	 * @param type Type that want to be serialized
	 * @return true: the type is streamable.
	 *         false: This is not possible
	 */
	public static boolean hasInputStream(Class<? extends Object> type) {
		Boolean isProperty = hasOutputStream.get(type);
		if (isProperty == null) {
			isProperty = DataStreamManager.createOutputStream(type) != null;
			hasOutputStream.put(type, isProperty);
		}
		return isProperty;
	}

	/**
	 * Verify if the type is streamable (in the register list only.
	 * @param type Type that want to be serialized
	 * @return true: the type is streamable.
	 *         false: This is not possible
	 */
	public static boolean hasIntputStreamOnly(Class<? extends Object> type) {
		return inputStreams.get(type) != null;
	}

	/**
	 * Verify if the type is streamable.
	 * @param type Type that want to be serialized
	 * @return true: the type is streamable.
	 *         false: This is not possible
	 */
	public static boolean hasOutputStream(Class<? extends Object> type) {
		Boolean isProperty = hasInputStream.get(type);
		if (isProperty == null) {
			isProperty = DataStreamManager.createOutputStream(type) != null;
			hasInputStream.put(type, isProperty);
		}
		return isProperty;
	}

	/**
	 * Register a new streamer for a specific type.
	 * @param type Type that can be stream with this streamer.
	 * @param inputStream Class that can read serialized data.
	 * @param outputStream Class that can generate serialized data.
	 * @param <T> Type of the class that can be streamable
	 */
	public static <T> void registerStream(Class<T> type, Class<? extends DataFlowInputStream<T>> inputStream, Class<? extends DataFlowOutputStream<T>> outputStream) {
		if (type == null || inputStream == null && outputStream == null)
			throw new IllegalArgumentException("Type, inputStream and outputStream cannot be null");
		if (inputStream != null)
			registerStreamInput(type, inputStream);
		if (outputStream != null)
			registerStreamOutput(type, outputStream);
	}

	/**
	 * Remove serializer for a specific type
	 * @param type Type that the serialization want to be removed
	 */
	public static void unregisterStream(Class<?> type) {
		unregisterStreamOutput(type);
		unregisterStreamInput(type);
	}

	/**
	 * Remove all serializator that is depending for a specific module
	 * @param module Module that have been removed
	 */
	public static void purgeStream(Module module) {
		purgeStreamOutput(module);
		purgeStreamInput(module);
	}

	/**
	 * Register output streamer
	 * @param typeClass Type of object that can be streamed
	 * @param outputStream Class that can serialize type
	 */
	private static <T> void registerStreamOutput(Class<T> typeClass, Class<? extends DataFlowOutputStream<T>> outputStream) {
		outputStreams.put(typeClass, outputStream);
		hasOutputStream = new HashMap<>();
	}

	/**
	 * Remove output streamer
	 * @param typeClass Type of object that must not be streamable anymore
	 */
	private static void unregisterStreamOutput(Class<?> typeClass) {
		outputStreams.remove(typeClass);
		// clear all the cache of research of available streamer
		hasOutputStream = new HashMap<>();
	}

	/**
	 * Remove all output streamer associated with this streamer
	 * @param module Module that must be purged
	 */
	private static void purgeStreamOutput(Module module) {
		outputStreams.keySet().removeIf(c -> c.getModule().equals(module));
		outputStreams.values().removeIf(c -> c.getModule().equals(module));
		// clear all the cache of research of available streamer
		hasOutputStream = new HashMap<>();
	}

	/**
	 * Register input streamer
	 * @param typeClass Type of object that can be streamed
	 * @param inputStream Class that can serialize type
	 */
	private static <T> void registerStreamInput(Class<T> typeClass, Class<? extends DataFlowInputStream<T>> inputStream) {
		inputStreams.put(typeClass, inputStream);
		hasInputStream = new HashMap<>();
	}

	/**
	 * Remove input streamer
	 * @param typeClass Type of object that must not be streamable anymore
	 */
	private static void unregisterStreamInput(Class<?> typeClass) {
		inputStreams.remove(typeClass);
		// clear all the cache of research of available streamer
		hasInputStream = new HashMap<>();
	}

	/**
	 * Remove all input streamer associated with this streamer
	 * @param module Module that must be purged
	 */
	private static void purgeStreamInput(Module module) {
		inputStreams.keySet().removeIf(c -> c.getModule().equals(module));
		inputStreams.values().removeIf(c -> c.getModule().equals(module));
		// clear all the cache of research of available streamer
		hasInputStream = new HashMap<>();
	}
}
