/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.datastream.input;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class SerializableInputStream<T> extends GenericDataFlowInputStream<T> {

	@SuppressWarnings("unchecked")
	@Override
	public T pop() throws IOException {
		// Note: If someone know a method to userialize with DataInput we agree any suggestion...
		try {
			// Read the number of data
			int size = this.dataInput.readInt();
			// read all the data
			byte[] data = new byte[size];
			this.dataInput.readFully(data);
			// un-serialize the data
			ByteArrayInputStream bos = new ByteArrayInputStream(data);
			ObjectInputStream ois = new ObjectInputStream(bos);
			Object out = ois.readUnshared();
			// TODO maybe verify the type of the object... but I do not know how "if (out instanceof T.class)" does not work
			return (T) out;
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		return null;
	}

}
