/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.datastream.input;

import java.io.DataInput;

public abstract class GenericDataFlowInputStream<T> implements DataFlowInputStream<T> {
	public DataInput dataInput;

	@Override
	public void setDataInput(DataInput dataInput) {
		this.dataInput = dataInput;
	}

}
