/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.datastream.input;

import java.io.DataInput;
import java.io.IOException;

/**
 * Interface to record a generic un-serializer for a specific type
 * @param <T> Type of object to serialize
 */
public interface DataFlowInputStream<T> {
	/**
	 * Set the streaming input for the current object decoder.
	 * @param dataInput Data stream where to read the data.
	 */
	void setDataInput(DataInput dataInput);

	/**
	 * Retrieve the next object in the stream
	 * @return The object in the stream un-serialized. The object can be null.
	 * @throws IOException Get an error in the stream.
	 */
	T pop() throws IOException;

}
