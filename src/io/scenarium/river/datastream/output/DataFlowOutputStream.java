/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.datastream.output;

import java.io.DataOutput;
import java.io.IOException;

/**
 * Interface to record a generic serializer for a specific type
 * @param <T> Type of object to serialize
 */
public interface DataFlowOutputStream<T> {
	/**
	 * Set the streaming output for the current object decoder.
	 * @param stream Data stream where to read the data.
	 */
	void setDataOutput(DataOutput stream);

	/**
	 * Push an object in the current stream.
	 * @param object Object to stream in the stream.
	 * @throws IOException Get an error while writing in the stream.
	 */
	void push(T object) throws IOException;

	/**
	 * Push an object (not type) in the current stream. The Object MUST have the type of template.
	 * @param value Object to stream in the stream.
	 * @throws IOException Get an error while writing in the stream.
	 */
	void pushObject(Object value) throws IOException;

}
