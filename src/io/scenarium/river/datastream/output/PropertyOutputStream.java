/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.datastream.output;

import java.io.IOException;

import io.beanmanager.editors.PropertyEditor;

public class PropertyOutputStream<T> extends GenericDataFlowOutputStream<T> {
	private final PropertyEditor<T> editor;

	public PropertyOutputStream(PropertyEditor<T> editor) {
		this.editor = editor;
	}

	public PropertyEditor<T> getEditor() {
		return this.editor;
	}

	@Override
	public void push(T value) throws IOException {
		this.editor.writeValue(this.dataOutput, value);
	}
}
