/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.datastream.output;

import java.io.DataOutput;
import java.io.IOException;

public abstract class GenericDataFlowOutputStream<T> implements DataFlowOutputStream<T> {
	public DataOutput dataOutput;

	@Override
	@SuppressWarnings("unchecked")
	public void pushObject(Object value) throws IOException {
		push((T) value);
	}

	@Override
	public void setDataOutput(DataOutput dataOutput) {
		this.dataOutput = dataOutput;
	}

}
