/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.datastream.output;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializableOutputStream<T> extends GenericDataFlowOutputStream<T> {

	@Override
	public void push(T value) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bos);
		oos.writeUnshared(value);
		// write the size of the data.
		this.dataOutput.writeInt(bos.size());
		// Write all the data.
		this.dataOutput.write(bos.toByteArray());
	}
}
