package io.scenarium.river;

import io.scenarium.river.consumer.DataStreamConsumer;
import io.scenarium.river.consumer.FileStreamConsumer;

/** Interface for the plug-in manager to provide streamer for data or file */
public interface PluginsStreamSupplier {

	/**
	 * Override this function to be called when the plug-in is started and register the data streamer.
	 * @param dataStreamConsumer Consumer that register the streamer
	 */
	default void populateDataStream(DataStreamConsumer dataStreamConsumer) {}

	/**
	 * Override this function to be called when the plug-in is started and register the file streamer (recorder and reader).
	 * @param fileStreamConsumer Consumer that register the file streamer
	 */
	default void populateFileStream(FileStreamConsumer fileStreamConsumer) {}
}
