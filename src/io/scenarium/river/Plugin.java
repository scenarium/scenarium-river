package io.scenarium.river;

import io.scenarium.pluginManager.PluginsSupplier;
import io.scenarium.river.consumer.DataStreamConsumer;
import io.scenarium.river.consumer.FileStreamConsumer;
import io.scenarium.river.datastream.DataStreamManager;
import io.scenarium.river.filerecorder.FileStreamManager;
import io.scenarium.river.filerecorder.recorder.FileStreamRecorder;
import io.scenarium.river.internal.Log;

public class Plugin implements PluginsSupplier {

	@Override
	public void birth() {
		// We must do this kind of things to be sure that no external dynamic module have create the class
		// in his path, and the instance in well managed by the plug-in.
		try {
			Class.forName(DataStreamManager.class.getName());
			Class.forName(FileStreamRecorder.class.getName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void loadPlugin(Object pluginInterface) {
		Log.print("Load the scenarium-river plugin on " + pluginInterface.getClass().getCanonicalName());
		if (pluginInterface instanceof PluginsStreamSupplier) {
			// TODO REFACTO move this in StreamIO plug-in
			PluginsStreamSupplier plugins = (PluginsStreamSupplier) pluginInterface;
			plugins.populateDataStream(new DataStreamConsumer());
			plugins.populateFileStream(new FileStreamConsumer());
		}
	}

	@Override
	public void unregisterModule(Module module) {
		// VII) DataStreams
		DataStreamManager.purgeStream(module);
		// VIII) StreamRecorders
		FileStreamManager.purgeStream(module);
	}

}
