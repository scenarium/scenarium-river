/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.filerecorder.reader;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.channels.FileChannel;

import io.scenarium.river.datastream.input.DataFlowInputStream;

// For the object that is streamable
public class StreamableObjectStreamReader extends GenericFileStreamReader {
	private final DataFlowInputStream<?> dfis;
	private DataInputStream pitchDis;
	private DataInputStream dataDis;
	private FileChannel channel;
	private long fileHeaderSize = -1;
	private FileChannel channelIndex;

	public StreamableObjectStreamReader(DataFlowInputStream<?> dfis) {
		super(EXTENTION_DATA_NAME);
		this.dfis = dfis;
	}

	@Override
	public File getFile() {
		File out = super.getFile();
		// get the header file size (only one time)
		if (this.fileHeaderSize == -1)
			try {
				this.fileHeaderSize = GenericFileStreamReader.getRecordFileHeaderSize(out);
			} catch (IOException e) {
				e.printStackTrace();
				this.fileHeaderSize = 0;
			}
		return out;
	}

	@Override
	public void close() throws IOException {
		IOException exception = null;
		if (this.pitchDis != null)
			try {
				this.pitchDis.close();
			} catch (IOException e) {
				e.printStackTrace();
				exception = e;
			}
		// if (this.oss != null)
		// try {
		// this.oss.close();
		// } catch (IOException e) {
		// e.printStackTrace();
		// exception = e;
		// }
		this.channel = null;
		if (exception != null)
			throw exception;
	}

	@Override
	public Object pop() throws IOException, FileNotFoundException {
		if (this.pitchDis == null) {
			FileInputStream fis = new FileInputStream(getFile());
			this.channel = fis.getChannel();
			// remove header
			this.channel.position(this.fileHeaderSize);
			this.dataDis = new DataInputStream(fis);
			this.dfis.setDataInput(this.dataDis);
			// open data index elements
			FileInputStream fisIndex = new FileInputStream(getFileIndex());
			this.channelIndex = fisIndex.getChannel();
			this.pitchDis = new DataInputStream(fisIndex);
		}
		Object out = this.dfis.pop();
		this.position++;
		return out;
	}

	@Override
	public void seek(long frame) throws IOException {
		if (this.channelIndex == null)
			return;
		this.channelIndex.position(Long.BYTES * frame);
		long filePosition = this.pitchDis.readLong();
		if (this.channel != null)
			this.channel.position(filePosition);
		this.position = frame;
	}

	@Override
	public long size() {
		// TODO If the file index is not present ==> create a new one with re-indexing...
		return getFileIndex().length() / Long.BYTES;
	}

}
