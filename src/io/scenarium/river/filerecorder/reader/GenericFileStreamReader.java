/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.filerecorder.reader;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import io.scenarium.river.filerecorder.FileStream;

public abstract class GenericFileStreamReader extends FileStream implements FileStreamReader {

	public GenericFileStreamReader(String extention) {
		super(extention);
	}

	public static long getRecordFileHeaderSize(File file) throws IOException {
		try (RandomAccessFile rafData = new RandomAccessFile(file, "r")) {
			// header is the first line :
			rafData.readLine();
			return rafData.getFilePointer();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new IOException("Can not get header of '" + file + "'");
		}
	}

}
