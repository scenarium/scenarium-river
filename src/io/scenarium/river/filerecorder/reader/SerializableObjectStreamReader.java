/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.filerecorder.reader;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.channels.FileChannel;

// toujouts utile pour les objets uniquement serializable
public class SerializableObjectStreamReader extends GenericFileStreamReader {
	private ObjectInputStream ois;
	private DataInputStream pitchDis;
	private DataInputStream dataDis;
	private FileChannel channel;
	private FileChannel channelIndex;
	private long fileHeaderSize = -1;

	public SerializableObjectStreamReader() {
		super(EXTENTION_DATA_NAME);
	}

	@Override
	public File getFile() {
		File out = super.getFile();
		// get the header file size (only one time)
		if (this.fileHeaderSize == -1)
			try {
				this.fileHeaderSize = GenericFileStreamReader.getRecordFileHeaderSize(out);
			} catch (IOException e) {
				e.printStackTrace();
				this.fileHeaderSize = 0;
			}
		return out;
	}

	@Override
	public void close() throws IOException {
		IOException exception = null;
		if (this.pitchDis != null)
			try {
				this.pitchDis.close();
			} catch (IOException e) {
				e.printStackTrace();
				exception = e;
			}
		if (this.ois != null)
			try {
				this.ois.close();
			} catch (IOException e) {
				e.printStackTrace();
				exception = e;
			}
		this.channel = null;
		if (exception != null)
			throw exception;
	}

	@Override
	public Object pop() throws IOException, FileNotFoundException {
		if (this.ois == null) {
			FileInputStream fos = new FileInputStream(getFile());
			this.channel = fos.getChannel();
			// remove header
			this.channel.position(this.fileHeaderSize);
			this.dataDis = new DataInputStream(fos);
			this.ois = new ObjectInputStream(this.dataDis);
			// open data index elements
			FileInputStream fisIndex = new FileInputStream(getFileIndex());
			this.channelIndex = fisIndex.getChannel();
			this.pitchDis = new DataInputStream(fisIndex);
		}
		try {
			Object out = this.ois.readUnshared();
			// this.ois.reset();
			this.position++;
			return out;
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	@Override
	public void seek(long frame) throws IOException {
		if (this.channelIndex == null)
			return;
		this.channelIndex.position(Long.BYTES * frame);
		long filePosition = this.pitchDis.readLong();
		if (this.channel != null)
			this.channel.position(filePosition);
		this.position = frame;
	}

	@Override
	public long size() {
		// TODO If the file index is not present ==> create a new one with re-indexing...
		return getFileIndex().length() / Long.BYTES;
	}

}
