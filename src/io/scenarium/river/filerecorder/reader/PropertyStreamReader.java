/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.filerecorder.reader;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.channels.FileChannel;

import io.beanmanager.editors.PropertyEditor;

public class PropertyStreamReader extends GenericFileStreamReader {
	private final PropertyEditor<?> streamableEditor;
	private DataInputStream dataDis;
	private DataInputStream pitchDis;
	private FileChannel channel;
	private long fileHeaderSize = -1;
	private FileChannel channelIndex;

	public PropertyStreamReader(PropertyEditor<?> streamableEditor) {
		super(EXTENTION_DATA_NAME);
		this.streamableEditor = streamableEditor;
	}

	@Override
	public File getFile() {
		File out = super.getFile();
		// get the header file size (only one time)
		if (this.fileHeaderSize == -1)
			try {
				this.fileHeaderSize = GenericFileStreamReader.getRecordFileHeaderSize(out);
			} catch (IOException e) {
				e.printStackTrace();
				this.fileHeaderSize = 0;
			}
		return out;
	}

	@Override
	public void close() {
		if (this.dataDis != null)
			try {
				this.dataDis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		if (this.pitchDis != null)
			try {
				this.pitchDis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		this.channel = null;
		this.channelIndex = null;
	}

	@Override
	public Object pop() throws IOException, FileNotFoundException {
		if (this.dataDis == null) {
			FileInputStream fis = new FileInputStream(getFile());
			this.channel = fis.getChannel();
			// remove header
			this.channel.position(this.fileHeaderSize);
			this.dataDis = new DataInputStream(fis);
			if (this.streamableEditor.getPitch() <= 0) {
				FileInputStream fisIndex = new FileInputStream(getFileIndex());
				this.channelIndex = fisIndex.getChannel();
				this.pitchDis = new DataInputStream(fisIndex);
			}
		}
		Object out = this.streamableEditor.readValue(this.dataDis);
		this.position++;
		return out;
	}

	@Override
	public void seek(long frame) throws IOException {
		long filePosition = 0;
		if (this.streamableEditor.getPitch() > 0)
			filePosition = this.fileHeaderSize + this.streamableEditor.getPitch() * frame;
		else {
			if (this.channelIndex == null)
				return;
			this.channelIndex.position(Long.BYTES * frame);
			filePosition = this.pitchDis.readLong();
		}
		if (this.channel != null)
			this.channel.position(filePosition);
		this.position = frame;
	}

	@Override
	public long size() {
		if (this.streamableEditor.getPitch() > 0)
			return (getFile().length() - this.fileHeaderSize) / this.streamableEditor.getPitch();
		else
			return getFileIndex().length() / Long.BYTES;

	}

}
