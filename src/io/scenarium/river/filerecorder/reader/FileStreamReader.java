/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.filerecorder.reader;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface FileStreamReader extends AutoCloseable {

	/**
	 * Set name of the file to record
	 * @param name Name of the file.
	 */
	void setName(String name);

	/**
	 * Get name of the file to record
	 * @return Name of the file.
	 */
	String getName();

	/**
	 * Set the folder of the recording directory.
	 * @param path Path of the recording directory.
	 */
	void setPath(String path);

	/**
	 * Get the folder of the recording directory.
	 * @return Path of the recording directory.
	 */
	String getPath();

	/**
	 * Get the extension of the file
	 * @return string of the extension (without the dot)
	 */
	String getExtention();

	/**
	 * Get the number of Object in the file.
	 * @return Count of all object in the file.
	 */
	long size();

	/**
	 * Move to a specific position in the file (number of object)
	 * @param position Id of the object (start at 0)
	 * @throws IOException Error in file seeking
	 */
	void seek(long position) throws IOException;

	/**
	 * Get the current position in the file
	 * @return Id of the current object.
	 */
	public long getPosition();

	/**
	 * Retrieve the Object at the current position (start at 0)
	 * @return the current un-serialized object. (type is same as the file generic type)
	 * @throws IOException  error in reading the data.
	 * @throws FileNotFoundException The file does not exist.
	 */
	Object pop() throws IOException, FileNotFoundException;
}
