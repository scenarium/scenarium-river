/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.filerecorder;

import java.io.File;

import io.scenarium.river.internal.Log;

public abstract class FileStream {
	/** Generic type of file for scenarium data index of elements. */
	protected static final String EXTENTION_INDEX_NAME = "pindex";
	/** Generic type of file for scenarium data. */
	protected static final String EXTENTION_DATA_NAME = "ppd";
	protected String path;
	protected String name;
	protected long position = 0;
	protected File file;
	protected File fileIndex;
	protected final String extension;

	public FileStream(String extension) {
		// remove the unneeded "." at the start of the extension
		while (extension.startsWith("."))
			extension = extension.substring(1);
		this.extension = extension;
	}

	public File getFile() {
		if (this.file == null)
			this.file = new File(this.path + File.separator + this.name + "." + this.extension);
		return this.file;
	}

	public File getFileIndex() {
		if (this.fileIndex == null)
			this.fileIndex = new File(this.path + File.separator + this.name + "." + EXTENTION_INDEX_NAME);
		return this.fileIndex;
	}

	public long getPosition() {
		return this.position;
	}

	public void setName(String name) {
		Log.info("Set name : '" + this.path + "'");
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setPath(String path) {
		Log.info("Set Path : '" + path + "'");
		this.path = path;
	}

	public String getPath() {
		return this.path;
	}

	public String getExtention() {
		return this.extension;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " -> " + this.name;
	}

}
