/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.filerecorder;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import io.beanmanager.BeanManager;
import io.beanmanager.editors.PropertyEditor;
import io.beanmanager.editors.PropertyEditorManager;
import io.beanmanager.editors.container.BeanEditor;
import io.scenarium.river.datastream.DataStreamManager;
import io.scenarium.river.datastream.input.DataFlowInputStream;
import io.scenarium.river.datastream.output.DataFlowOutputStream;
import io.scenarium.river.filerecorder.reader.FileStreamReader;
import io.scenarium.river.filerecorder.reader.PropertyStreamReader;
import io.scenarium.river.filerecorder.reader.SerializableObjectStreamReader;
import io.scenarium.river.filerecorder.reader.StreamableObjectStreamReader;
import io.scenarium.river.filerecorder.recorder.FileStreamRecorder;
import io.scenarium.river.filerecorder.recorder.PropertyStreamRecorder;
import io.scenarium.river.filerecorder.recorder.SerializableObjectStreamRecorder;
import io.scenarium.river.filerecorder.recorder.StreamableObjectStreamRecorder;

/** This Singleton permit to get a recorder/reader associated with a specific Type of class */
public abstract class FileStreamManager {
	// This class can not be allocated
	private FileStreamManager() {}

	/** List of file RECORDER register by the user */
	private static ConcurrentHashMap<Class<?>, Class<? extends FileStreamRecorder>> recorderStreams = new ConcurrentHashMap<>();
	/** List of file READER register by the user */
	private static ConcurrentHashMap<Class<?>, Class<? extends FileStreamReader>> readerStreams = new ConcurrentHashMap<>();
	/** Associated extension with class */
	private static ConcurrentHashMap<String, Class<?>> extensionStreams = new ConcurrentHashMap<>();

	/**
	 * Create a recorder associated with the request type.
	 * Search in (in sequence):
	 *   - The local recorder available
	 *   - a Streamer available
	 *   - A property editor from bean manager (not a BeanEditor)
	 *   - A serializable object recorder
	 *   - null
	 * @param typeClass Type that want to be recorded
	 * @return An implementation of an recorder of this specific class.
	 */
	public static FileStreamRecorder getRecorder(Class<?> typeClass) {
		// [1] try to find recorder register in local
		Class<? extends FileStreamRecorder> recorder = recorderStreams.get(typeClass);
		if (recorder != null)
			try {
				return recorder.getConstructor().newInstance();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				return null;
			}
		// [2] try to find recorder in the streamer
		DataFlowOutputStream<?> os = DataStreamManager.createOutputStreamOnly(typeClass);
		if (os != null)
			return new StreamableObjectStreamRecorder(os);
		// [3] try to find recorder in the property editor
		PropertyEditor<?> pe = PropertyEditorManager.findEditor(typeClass, "");
		if (pe instanceof BeanEditor<?>)
			pe = null;
		if (pe != null)
			return new PropertyStreamRecorder(pe);
		// [4] If type is serialisable, create custom recorder
		if (Serializable.class.isAssignableFrom(typeClass))
			return new SerializableObjectStreamRecorder();
		// [5] do NOT find a recorder
		return null;
	}

	/**
	 * Check if a recorder is associated with the request type.
	 * Search in (in sequence):
	 *   - The local recorder available
	 *   - a Streamer available
	 *   - A property editor from bean manager (not a BeanEditor)
	 *   - A serializable object recorder
	 *   - null
	 * @param typeClass Type that want to be recorded
	 * @return True if a recorder is available, false ortherwise.
	 */
	public static boolean hasStreamRecorder(Class<?> typeClass) {
		// [1] try to find recorder register in local
		if (recorderStreams.get(typeClass) != null)
			return true;
		// [2] try to find recorder in the streamer
		if (DataStreamManager.hasOutputStreamOnly(typeClass))
			return true;
		// [3] try to find recorder in the property editor
		PropertyEditor<?> pe = PropertyEditorManager.findEditor(typeClass, "");
		if (pe instanceof BeanEditor<?>)
			pe = null;
		if (pe != null)
			return true;
		// [4] If type is Serializable, create custom recorder
		if (Serializable.class.isAssignableFrom(typeClass))
			return true;
		return false;
	}

	/**
	 * Register a new recorder/reader in the system
	 * @param typeClass Type of class that must be read and record
	 * @param extention Extension of the file
	 * @param streamReaderClass Reading class
	 * @param streamRecordClass Writing class
	 */
	public static void registerStream(Class<?> typeClass, String extention, Class<? extends FileStreamReader> streamReaderClass, Class<? extends FileStreamRecorder> streamRecordClass) {
		if (typeClass == null || extention == null || streamReaderClass == null && streamRecordClass == null)
			throw new IllegalArgumentException("Type, inputStream and outputStream cannot be null");
		if (extensionStreams.containsKey(extention))
			throw new IllegalArgumentException("extention " + extention + " is already registered.");
		extensionStreams.put(extention, typeClass);
		if (streamReaderClass != null)
			registerStreamReader(typeClass, streamReaderClass);
		if (streamRecordClass != null)
			registerStreamRecorder(typeClass, streamRecordClass);
	}

	/**
	 * Remove a Type from the file streamer
	 * @param typeClass Type to remove
	 */
	public static void unregisterStream(Class<?> typeClass) {
		unregisterStreamRecorder(typeClass);
		unregisterStreamReader(typeClass);
	}

	/**
	 * Purge all reference of all the module in parameter
	 * @param module Module that might be purged
	 */
	public static void purgeStream(Module module) {
		purgeStreamReader(module);
		purgeStreamRecorder(module);
		purgeExtention(module);
	}

	/**
	 * Create a reader associated with the request type.
	 * Search in (in sequence):
	 *   - The local reader available
	 *   - a Streamer available
	 *   - A property editor from bean manager (not a BeanEditor)
	 *   - A serializable object reader
	 *   - null
	 * @param typeClass Type that want to be recorded
	 * @return An implementation of an recorder of this specific class.
	 */
	public static FileStreamReader getReader(Class<?> typeClass) {
		// [1] try to find recorder register in local
		Class<? extends FileStreamReader> reader = readerStreams.get(typeClass);
		if (reader != null)
			try {
				return reader.getConstructor().newInstance();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				return null;
			}
		// [2] try to find recorder in the streamer
		DataFlowInputStream<?> os = DataStreamManager.createInputStreamOnly(typeClass);
		if (os != null)
			return new StreamableObjectStreamReader(os);
		// [3] try to find recorder in the property editor
		PropertyEditor<?> pe = PropertyEditorManager.findEditor(typeClass, "");
		if (pe instanceof BeanEditor<?>)
			pe = null;
		if (pe != null)
			return new PropertyStreamReader(pe);
		// [4] If type is serialisable, create custom recorder
		if (Serializable.class.isAssignableFrom(typeClass))
			return new SerializableObjectStreamReader();
		// [5] do NOT find a reader
		return null;
	}

	public static boolean hasStreamReader(Class<?> typeClass) {
		// [1] try to find recorder register in local
		if (readerStreams.get(typeClass) != null)
			return true;
		// [2] try to find recorder in the streamer
		if (DataStreamManager.hasOutputStreamOnly(typeClass))
			return true;
		// [3] try to find recorder in the property editor
		PropertyEditor<?> pe = PropertyEditorManager.findEditor(typeClass, "");
		if (pe instanceof BeanEditor<?>)
			pe = null;
		if (pe != null)
			return true;
		// [4] If type is serialisable, create custom recorder
		if (Serializable.class.isAssignableFrom(typeClass))
			return true;
		// [5] do NOT find a reader
		return false;
	}

	/**
	 * Remove all reference of the Modules in the extension
	 * @param module Module that might be purged
	 */
	private static void purgeExtention(Module module) {
		extensionStreams.values().removeIf(c -> c.getModule().equals(module));
	}

	/**
	 * Register a recorder for a specific type
	 * @param typeClass Type to register
	 * @param streamRecordClass Recorder class of this type
	 */
	private static void registerStreamRecorder(Class<?> typeClass, Class<? extends FileStreamRecorder> streamRecordClass) {
		recorderStreams.put(typeClass, streamRecordClass);
	}

	/**
	 * Remove all recorder of this type
	 * @param typeClass Type to remove
	 */
	private static void unregisterStreamRecorder(Class<?> typeClass) {
		recorderStreams.remove(typeClass);
	}

	/**
	 * Purge all the reference of this module in recorders
	 * @param module Module to purge
	 */
	private static void purgeStreamRecorder(Module module) {
		recorderStreams.keySet().removeIf(c -> c.getModule().equals(module));
		recorderStreams.values().removeIf(c -> c.getModule().equals(module));
	}

	/**
	 * Register a reader for a specific type
	 * @param typeClass Type to register
	 * @param streamRecordClass reader class of this type
	 */
	private static void registerStreamReader(Class<?> typeClass, Class<? extends FileStreamReader> streamClass) {
		readerStreams.put(typeClass, streamClass);
	}

	/**
	 * Remove all reader of this type
	 * @param typeClass Type to remove
	 */
	private static void unregisterStreamReader(Class<?> typeClass) {
		readerStreams.remove(typeClass);
		extensionStreams.values().removeIf(c -> c.equals(typeClass));
	}

	/**
	 * Purge all the reference of this module in readers
	 * @param module Module to purge
	 */
	private static void purgeStreamReader(Module module) {
		readerStreams.keySet().removeIf(c -> c.getModule().equals(module));
		readerStreams.values().removeIf(c -> c.getModule().equals(module));
	}

	/**
	 * Get the type of class recorded in the file.
	 * @param file File to inspect
	 * @return Class reference of the file
	 * @throws IOException If the file does not exist
	 */
	public static Class<?> getRecordFileClass(File file) throws IOException {
		String fileName = file.getName();
		String extention = fileName.substring(fileName.lastIndexOf(".") + 1);
		Class<?> out = extensionStreams.get(extention);
		if (out == null || extention.contentEquals(FileStream.EXTENTION_DATA_NAME))
			try (RandomAccessFile rafData = new RandomAccessFile(file, "r")) {
				String ligne = rafData.readLine();
				rafData.close();
				out = BeanManager.getClassFromDescriptor(ligne);
			} catch (Exception ex) {
				ex.printStackTrace();
				throw new IOException("Can not extract record file type from file '" + file + "'");
			}
		return out;
	}

	/**
	 * Get all the extension readable by the recorder
	 * @return List of all extension.
	 */
	public static String[] getReaderFormatNames() {
		Set<String> list = extensionStreams.keySet();
		String[] values = new String[list.size() + 1];
		int iii = 0;
		for (String elem : list)
			values[iii + 1] = elem;
		values[0] = "ppd";
		return values;
	}
}
