/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.filerecorder.recorder;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.channels.FileChannel;

import io.beanmanager.BeanManager;

// toujouts utile pour les objets uniquement serializable
public class SerializableObjectStreamRecorder extends GenericFileStreamRecorder {
	private ObjectOutputStream oos;
	private DataOutputStream pitchDos;
	private DataOutputStream dataDos;
	private FileChannel channel;

	public SerializableObjectStreamRecorder() {
		super(EXTENTION_DATA_NAME);
	}

	@Override
	public void close() throws IOException {
		IOException exception = null;
		if (this.pitchDos != null)
			try {
				this.pitchDos.close();
			} catch (IOException e) {
				e.printStackTrace();
				exception = e;
			}
		if (this.oos != null)
			try {
				this.oos.close();
			} catch (IOException e) {
				e.printStackTrace();
				exception = e;
			}
		this.channel = null;
		this.position = 0;
		if (exception != null)
			throw exception;
	}

	@Override
	public void push(Object value) throws IOException, FileNotFoundException {
		if (this.oos == null) {
			FileOutputStream fos = new FileOutputStream(getFile());
			this.channel = fos.getChannel();
			this.dataDos = new DataOutputStream(fos);
			this.dataDos.writeBytes(BeanManager.getDescriptorFromClass(value.getClass()) + System.lineSeparator());
			this.oos = new ObjectOutputStream(this.dataDos);
			this.pitchDos = new DataOutputStream(new FileOutputStream(getFileIndex()));
		}
		if (this.pitchDos != null)
			this.pitchDos.writeLong(this.channel.position()); // dataDos.size() marche, pas, il est en int...
		this.oos.writeUnshared(value);
		this.oos.reset();
		this.position++;
	}
}
