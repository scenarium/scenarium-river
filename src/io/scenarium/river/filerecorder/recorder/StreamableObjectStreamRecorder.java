/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.filerecorder.recorder;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import io.beanmanager.BeanManager;
import io.scenarium.river.datastream.output.DataFlowOutputStream;

// For the object that is streamable
public class StreamableObjectStreamRecorder extends GenericFileStreamRecorder {
	private final DataFlowOutputStream<?> dfos;
	private DataOutputStream pitchDos;
	private DataOutputStream dataDos;
	private FileChannel channel;

	public StreamableObjectStreamRecorder(DataFlowOutputStream<?> dfos) {
		super(EXTENTION_DATA_NAME);
		this.dfos = dfos;
	}

	@Override
	public void close() throws IOException {
		IOException exception = null;
		if (this.pitchDos != null)
			try {
				this.pitchDos.close();
			} catch (IOException e) {
				e.printStackTrace();
				exception = e;
			}
		if (this.dataDos != null)
			try {
				this.dataDos.close();
			} catch (IOException e) {
				e.printStackTrace();
				exception = e;
			}
		this.channel = null;
		this.position = 0;
		if (exception != null)
			throw exception;
	}

	@Override
	public void push(Object value) throws IOException, FileNotFoundException {
		if (this.dataDos == null) {
			FileOutputStream fos = new FileOutputStream(getFile());
			this.channel = fos.getChannel();
			this.dataDos = new DataOutputStream(fos);
			// StringEditor.writeString(dataDos, value.getClass().getName()); //TODO je dois faire ca normalement...
			this.dataDos.writeBytes(BeanManager.getDescriptorFromClass(value.getClass()) + System.lineSeparator());
			this.dfos.setDataOutput(this.dataDos);
			this.pitchDos = new DataOutputStream(new FileOutputStream(getFileIndex()));
		}
		if (this.pitchDos != null)
			this.pitchDos.writeLong(this.channel.position()); // dataDos.size() marche, pas, il est en int...
		this.dfos.pushObject(value);
		this.position++;
	}
}
