/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.filerecorder.recorder;

import io.scenarium.river.filerecorder.FileStream;

public abstract class GenericFileStreamRecorder extends FileStream implements FileStreamRecorder {

	public GenericFileStreamRecorder(String extension) {
		super(extension);
	}

}
