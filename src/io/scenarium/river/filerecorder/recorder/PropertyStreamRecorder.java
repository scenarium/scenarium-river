/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.filerecorder.recorder;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import io.beanmanager.BeanManager;
import io.beanmanager.editors.PropertyEditor;

public class PropertyStreamRecorder extends GenericFileStreamRecorder {
	private final PropertyEditor<?> streamableEditor;
	private DataOutputStream dataDos;
	private DataOutputStream pitchDos;
	private FileChannel channel;

	public PropertyStreamRecorder(PropertyEditor<?> streamableEditor) {
		super(EXTENTION_DATA_NAME);
		this.streamableEditor = streamableEditor;
	}

	@Override
	public void close() {
		if (this.dataDos != null)
			try {
				this.dataDos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		if (this.pitchDos != null)
			try {
				this.pitchDos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		this.position = 0;
		this.channel = null;
	}

	@Override
	public void push(Object object) throws IOException, FileNotFoundException {
		// Open the file stream and add the file header data
		if (this.dataDos == null) {
			FileOutputStream fos = new FileOutputStream(getFile());
			this.channel = fos.getChannel();
			this.dataDos = new DataOutputStream(fos);
			// TODO In theory I need to do this. but it work without it.
			// StringEditor.writeString(dataDos, object.getClass().getName());
			// Write the class name with a line separator at the end.
			this.dataDos.writeBytes(BeanManager.getDescriptorFromClass(object.getClass()) + System.lineSeparator());
			if (this.streamableEditor.getPitch() <= 0)
				// Open a pitch file that index every element position in the file
				this.pitchDos = new DataOutputStream(new FileOutputStream(getFileIndex()));
		}
		if (this.pitchDos != null)
			this.pitchDos.writeLong(this.channel.position()); // dataDos.size() marche, pas, il est en int...
		// Write the data
		this.streamableEditor.writeValueFromObj(this.dataDos, object);
		this.position++;
	}

}
