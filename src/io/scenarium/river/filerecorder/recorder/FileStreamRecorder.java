/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.river.filerecorder.recorder;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface FileStreamRecorder extends AutoCloseable {

	/**
	 * Set name of the file to record
	 * @param name Name of the file.
	 */
	void setName(String name);

	/**
	 * Get name of the file to record
	 * @return name Name of the file.
	 */
	String getName();

	/**
	 * Set the folder of the recording directory
	 * @param path Path of the recording directory
	 */
	void setPath(String path);

	/**
	 * Get the folder of the recording directory.
	 * @return Path of the recording directory.
	 */
	String getPath();

	/**
	 * Get the extension of the file
	 * @return string of the extension (without the dot)
	 */
	String getExtention();

	/**
	 * Get the current position in the file
	 * @return Id of the current object.
	 */
	public long getPosition();

	/**
	 * write a serialized Object in the file (same for all object in the file) in the
	 * @param object Object to write.
	 * @throws IOException Error in serialization of the object
	 * @throws FileNotFoundException Error while creating the file.
	 */
	void push(Object object) throws IOException, FileNotFoundException;
}
