import io.scenarium.pluginManager.PluginsSupplier;
import io.scenarium.river.Plugin;

open module io.scenarium.river {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	requires transitive io.beanmanager;
	requires java.base;
	requires vecmath;

	exports io.scenarium.river;
	exports io.scenarium.river.consumer;
	exports io.scenarium.river.datastream.input;
	exports io.scenarium.river.datastream.output;
	exports io.scenarium.river.datastream;
	exports io.scenarium.river.filerecorder;
	exports io.scenarium.river.filerecorder.reader;
	exports io.scenarium.river.filerecorder.recorder;
}
